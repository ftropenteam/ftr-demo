package eu.future.earth.client.demo;

import java.io.Serializable;

public class TestInfo implements Serializable {

    private String name = null;
    private long ownerId = -1;

    public TestInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }
}
