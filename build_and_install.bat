@echo off

call build.bat %2

ECHO Starting copy
set TEST=C:\servers\test-wildfly-20.0.0
xcopy demo-ear\target\*.ear %TEST%\standalone\deployments\demo-ear.ear /y

set TEST2=C:\servers\test-wildfly-21.0.0
xcopy demo-ear\target\*.ear %TEST2%\standalone\deployments\demo-ear.ear /y

@echo ON