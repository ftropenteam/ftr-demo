package eu.future.earth.client.demo;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public interface DemoRestClient extends RestService {

    @GET
    @Path("/batch/run/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void runBatch(@PathParam("id") final Long id, MethodCallback<TestInfo> callback);

}
