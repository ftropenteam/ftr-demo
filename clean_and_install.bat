@echo off

call buildAndClean.bat %2

ECHO Starting copy

copy ftr-crm-ear\target\ftr-crm-ear-%1.ear G:\servers\ftrcrm\jboss-5.1.0.GA\server\default\deploy\ftr-crm-ear.ear /y
copy ftr-crm-ear\target\ftr-crm-ear-%1.ear G:\servers\kiekeboe\jboss-5.1.0.GA\server\default\deploy\ftr-crm-ear.ear /y

copy ftr-crm-ear\target\ftr-crm-ear-%1.ear G:\servers\kiekeboe\jboss-as-7.1.1.Final\standalone\deployments\ftr-crm-ear.ear /y


@echo ON