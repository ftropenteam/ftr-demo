package eu.future.earth.crm.server.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class RestApplication extends Application {

	private Set<Object> singletons = new HashSet<Object>();

	private Set<Class<?>> empty = new HashSet<Class<?>>();

	public RestApplication() {
		this.singletons.add(new RestStatusApi());
		this.empty.add(RestStatusApi.class);
		this.empty.add(io.swagger.jaxrs.listing.ApiListingResource.class);
		this.empty.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return this.empty;
	}

	@Override
	public Set<Object> getSingletons() {
		return this.singletons;
	}
}
