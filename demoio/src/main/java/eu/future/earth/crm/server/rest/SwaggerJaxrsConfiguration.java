package eu.future.earth.crm.server.rest;

import io.swagger.jaxrs.config.BeanConfig;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet(name = "SwaggerJaxrsConfiguration", loadOnStartup = 1)
public class SwaggerJaxrsConfiguration extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6146632017062383358L;

	@Override
	public void init(ServletConfig servletConfig) {
		try {
			super.init(servletConfig);
			BeanConfig beanConfig = new BeanConfig();
			//			beanConfig.setS
			beanConfig.setVersion("1.0.2");
			beanConfig.setSchemes(new String[] {
					"http",
					"https"
			});
			//			beanConfig.setHost("localhost:8080");
			beanConfig.setTitle("The Crm Integration Interface");
			beanConfig.setBasePath("/demoio/rest");
			beanConfig.setResourcePackage("eu.future.earth.crm.server.rest");
			beanConfig.setScan(true);
		} catch (ServletException e) {
			System.out.println(e.getMessage());
		}
	}
}
