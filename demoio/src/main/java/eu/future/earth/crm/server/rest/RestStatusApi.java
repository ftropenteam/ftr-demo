package eu.future.earth.crm.server.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import eu.duo.dfk.test.SampleEjb;
import eu.future.earth.client.demo.TestInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("/secure/batch")
@Api(value = "The Status Api")
@Stateless
public class RestStatusApi {

	@EJB
	private SampleEjb dfkService = null;

	@Context
	private HttpServletResponse response;

	@Context
	private HttpServletRequest request;

	@GET
	@Path("/run/{id}")
	@Produces("application/json")
	@ApiOperation(value = "Get the status for a Dfk id", notes = "Return the servers.")
	public TestInfo runBatch(@ApiParam(value = "The Id", required = true) @PathParam("id") final String id) {
		System.out.println("sdfsdf" + request.getRemoteUser());
		TestInfo reg = new TestInfo();
		dfkService.register(reg);
		return reg;

	}
	

}
