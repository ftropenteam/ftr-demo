package eu.future.earth.crm.server.rest;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CorsFilter implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws java.io.IOException, ServletException {
		HttpServletRequest hreq = (HttpServletRequest) request;
		HttpServletResponse hres = (HttpServletResponse) response;
		if (!hres.containsHeader("Access-Control-Allow-Origin")) {
			hres.addHeader("Access-Control-Allow-Origin", "*");
			hres.addHeader("Access-Control-Allow-Methods", "POST, GET, UPDATE, OPTIONS");
			hres.addHeader("Access-Control-Allow-Headers", "x-http-method-override, Origin, X-Requested-With, Content-Type, Accept");
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}
}
