package eu.future.earth.gwt.client.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ExternalTextResource;

public interface HtmlInterface extends ClientBundle {

	@Source("HtmlDemoContent.html")
	ExternalTextResource messageSend();


}
