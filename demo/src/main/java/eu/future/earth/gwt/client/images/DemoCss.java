package eu.future.earth.gwt.client.images;

public class DemoCss {
	
	public final static String PREFIX = "ftrdemo-";

	public final static String EVENT_HEADER_MONDAY = PREFIX + "eventHeaderMonday";
	
	public final static String EVENT_PANEL_MONDAY = PREFIX + "eventBodyMonday";
	
	public final static String WHOLEDAY_PANEL_MONDAY = PREFIX + "eventWholeDayMonday";
	
	public final static String EVENT_HEADER_NORMAL = PREFIX + "eventHeaderNormal";
	
	public final static String EVENT_PANEL_NORMAL = PREFIX + "eventBodyNormal";
	
	public final static String WHOLEDAY_PANEL_NORMAL = PREFIX + "eventWholeDayNormal";
}
