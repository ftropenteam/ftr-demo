package eu.future.earth.gwt.server.servlet.test;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import eu.future.earth.client.LocationInfo;
import eu.future.earth.client.date.DateNoTimeZone;
import eu.future.earth.client.general.PaymentType;
import eu.future.earth.client.gps.GpsLocationImpl;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FinancialSettings extends GpsLocationImpl implements Serializable, LocationInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlAttribute
	private String street = null;

	@XmlAttribute
	private String town = null;

	@XmlAttribute
	private String houseNumber = null;

	@XmlAttribute
	private String houseNumberApendix = null;

	@XmlAttribute
	private String postalCode = null;

	@XmlAttribute
	private String registrationNumber = "--";

	@XmlAttribute
	private int sepaCountNumber = 0;

	@XmlAttribute
	private boolean otherNameForIncasso = false;

	@XmlAttribute
	private String nameForIncasso = null;

	public boolean isOtherNameForIncasso() {
		return otherNameForIncasso;
	}

	public void setOtherNameForIncasso(boolean otherNameForIncasso) {
		this.otherNameForIncasso = otherNameForIncasso;
	}

	public String getNameForIncasso() {
		return nameForIncasso;
	}

	public void setNameForIncasso(String nameForIncasso) {
		this.nameForIncasso = nameForIncasso;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	@XmlAttribute
	private int zoomLevel = 10;

	@XmlAttribute
	private String accountNumber = null;

	@XmlAttribute
	private PaymentType paymentType = null;

	@XmlAttribute
	private boolean mailInvoice = true;

	@XmlAttribute
	private boolean invoicePerChild = false;

	@XmlAttribute
	private long customerLocation = -1;

	@XmlAttribute
	private boolean printInvoice = true;

	@XmlAttribute
	private String ibanNumber = null;

	@XmlAttribute
	private String ibanBankBicNumber = null;

	@XmlAttribute
	private String ibanBankMandaat = null;

	@XmlAttribute
	private DateNoTimeZone ibanBankMandaatSignature = null;

	public String getIbanNumber() {
		return ibanNumber;
	}

	public void setIbanNumber(String ibanNumber) {
		this.ibanNumber = ibanNumber;
	}

	public String getIbanBankBicNumber() {
		return ibanBankBicNumber;
	}

	public void setIbanBankBicNumber(String ibanBankBicNumber) {
		this.ibanBankBicNumber = ibanBankBicNumber;
	}

	public String getIbanBankMandaat() {
		return ibanBankMandaat;
	}

	public void setIbanBankMandaat(String ibanBankMandaat) {
		this.ibanBankMandaat = ibanBankMandaat;
	}

	public boolean isPrintInvoice() {
		return printInvoice;
	}

	public void setPrintInvoice(boolean printInvoice) {
		this.printInvoice = printInvoice;
	}

	public FinancialSettings() {
		super();
	}

	public String getAdressLine01() {
		StringBuffer result = new StringBuffer();
		result.append(street);
		result.append(" ");
		result.append(houseNumber);
		if (houseNumberApendix != null && houseNumberApendix.length() > 0) {
			result.append(" ");
			result.append(houseNumberApendix);
		}
		return result.toString();
	}

	public String getAdressLine02() {
		StringBuffer result = new StringBuffer();
		result.append(postalCode);
		result.append(" ");
		result.append(town);
		return result.toString();
	}

	/**
	 * @return the street
	 */

	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the town
	 */

	public String getTown() {
		return town;
	}

	/**
	 * @param town
	 *            the town to set
	 */
	public void setTown(String town) {
		this.town = town;
	}

	/**
	 * @return the houseNumber
	 */

	public String getHouseNumber() {
		return houseNumber;
	}

	/**
	 * @param houseNumber
	 *            the houseNumber to set
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	/**
	 * @return the postalCode
	 */

	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public boolean isMailInvoice() {
		return mailInvoice;
	}

	public void setMailInvoice(boolean mailInvoice) {
		this.mailInvoice = mailInvoice;
	}

	public boolean isInvoicePerChild() {
		return invoicePerChild;
	}

	public void setInvoicePerChild(boolean invoicePerChild) {
		this.invoicePerChild = invoicePerChild;
	}

	public long getCustomerLocation() {
		return customerLocation;
	}

	public void setCustomerLocation(long customerLocation) {
		this.customerLocation = customerLocation;
	}

	public void setHouseNumberApendix(String houseNumberApendix) {
		this.houseNumberApendix = houseNumberApendix;
	}

	public String getHouseNumberApendix() {
		return houseNumberApendix;
	}

	public int getZoomLevel() {
		return zoomLevel;
	}

	public void setZoomLevel(int zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public int getSepaCountNumber() {
		return sepaCountNumber;
	}

	public void setSepaCountNumber(int sepaCountNumber) {
		this.sepaCountNumber = sepaCountNumber;
	}

	public DateNoTimeZone getIbanBankMandaatSignature() {
		return ibanBankMandaatSignature;
	}

	public void setIbanBankMandaatSignature(DateNoTimeZone ibanBankMandaatSignature) {
		this.ibanBankMandaatSignature = ibanBankMandaatSignature;
	}

}
