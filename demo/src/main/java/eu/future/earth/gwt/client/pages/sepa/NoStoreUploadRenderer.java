package eu.future.earth.gwt.client.pages.sepa;

import eu.future.earth.client.file.LinkHolder;
import eu.future.earth.gwt.client.communication.ServletConstants;
import eu.future.earth.gwt.client.file.FileLinkHolderRenderer;

public class NoStoreUploadRenderer implements FileLinkHolderRenderer<NoStoreUpload> {

	@Override
	public String getPrefix() {
		return ServletConstants.IMPORT_UPLOAD;
	}

	@Override
	public int getMaxAllowed() {
		return 1;
	}
	
	@Override
	public boolean showFileName() {
		return true;
	}

	@Override
	public LinkHolder getHolder(NoStoreUpload data) {
		return new LinkHolder();
	}

}
