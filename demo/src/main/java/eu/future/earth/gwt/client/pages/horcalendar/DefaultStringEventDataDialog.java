/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client.pages.horcalendar;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;

import eu.future.earth.gwt.client.DateEditFieldWithPicker;
import eu.future.earth.gwt.client.date.DateEvent;
import eu.future.earth.gwt.client.date.DateEvent.DateEventActions;
import eu.future.earth.gwt.client.date.DateEventListener;
import eu.future.earth.gwt.client.date.HasDateEventHandlers;
import eu.future.earth.gwt.client.date.horizontal.HorizontalDateRenderer;
import eu.future.earth.gwt.client.pages.agenda.DefaultEventData;
import eu.future.earth.gwt.client.time.TimeBox;
import eu.future.earth.gwt.client.ui.UiHelper;
import eu.future.earth.gwt.client.ui.button.CleanDialog;
import eu.future.earth.gwt.client.ui.panels.PanelWithMainAndButtonPanel;
import gwt.material.design.client.ui.MaterialButton;

public class DefaultStringEventDataDialog extends CleanDialog
		implements KeyPressHandler, ClickHandler, HasDateEventHandlers<DefaultEventData> {

	private TextArea text = new TextArea();

	private DateEditFieldWithPicker date;

	private CheckBox wholeDay = new CheckBox();

	private HorizontalPanel time = new HorizontalPanel();

	private HorizontalPanel timePanel = new HorizontalPanel();

	private TimeBox start;

	private TimeBox end;

	private MaterialButton cancel = null;

	private MaterialButton ok = null;

	private MaterialButton delete = null;

	private DefaultEventData data = null;

	private DateEventActions command = DateEventActions.ADD;

	public DefaultStringEventDataDialog(HorizontalDateRenderer<DefaultEventData, UserDemo> renderer,
			DefaultEventData newData) {
		this(renderer, newData, DateEventActions.ADD);
	}

	public DefaultStringEventDataDialog(HorizontalDateRenderer<DefaultEventData, UserDemo> renderer,
			DefaultEventData newData, DateEventActions newCommand) {
		super();

		date = new DateEditFieldWithPicker("MM/dd/yyyy");
		start = new TimeBox(renderer.show24HourClock() ? "HH:mm" : "hh:mmaa");
		end = new TimeBox(renderer.show24HourClock() ? "HH:mm" : "hh:mmaa");
		command = newCommand;
		data = newData;
		date.setDate(data.getStartTime());
		start.setValue(data.getStartTime());
		if (data.getEndTime() != null) {
			end.setValue(data.getEndTime());
			wholeDay.setValue(false);
		} else {
			wholeDay.setValue(true);
		}
		if (newCommand == DateEventActions.ADD) {
			setText("New Event");
		} else {
			text.setText(data.getData());
			setText("Edit Event");
		}

		// VerticalPanel outer = new VerticalPanel();

		final FlexTable table = new FlexTable();

		table.setWidget(0, 0, new Label("Datum"));
		table.setWidget(0, 1, date);

		timePanel.add(start);
		timePanel.add(new Label("-"));
		timePanel.add(end);

		time.add(wholeDay);
		wholeDay.addClickHandler(this);
		if (data.getEndTime() != null) {
			time.add(timePanel);
		}

		table.setWidget(0, 2, time);
		table.getFlexCellFormatter().setHorizontalAlignment(0, 2, HorizontalPanel.ALIGN_LEFT);
		table.setWidget(1, 0, new Label("Text"));

		table.setWidget(1, 1, text);
		table.getFlexCellFormatter().setColSpan(1, 1, 2);
		// outer.add(table);

		PanelWithMainAndButtonPanel main = new PanelWithMainAndButtonPanel(table);

		text.addKeyPressHandler(this);
		text.setWidth("250px");
		text.setHeight("100px");
		cancel = UiHelper.newMaterialButton("Cancel", this);

		cancel.setFocus(true);
		cancel.setAccessKey('c');

		ok = UiHelper.greenMaterialButton("Ok", this);
		ok.setEnabled(false);
		ok.setFocus(true);
		ok.setAccessKey('o');

		main.addButton(ok);

		if (command == DateEventActions.UPDATE) {
			delete = UiHelper.redMaterialButton("Delete", this);
			delete.setFocus(true);
			delete.setAccessKey('d');
			delete.addClickHandler(this);
			main.addButton(delete);
		}
		main.addButton(cancel);
		setWidget(table);
		if (text.getText().length() > 1) {
			ok.setEnabled(true);
		} else {
			ok.setEnabled(false);
		}
	}

	@Override
	public void onKeyPress(KeyPressEvent event) {
		if (text.getText().length() > 1) {
			ok.setEnabled(true);
		} else {
			ok.setEnabled(false);
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == wholeDay) {
			if (wholeDay.getValue()) {
				if (time.getWidgetIndex(timePanel) > -1) {
					time.remove(timePanel);
				}
			} else {
				if (time.getWidgetIndex(timePanel) == -1) {
					time.add(timePanel);
				}
			}
		} else {
			if (event.getSource() == ok) {
				if (data == null) {
					data = new DefaultEventData(System.currentTimeMillis());
				}
				if (wholeDay.getValue()) {
					data.setStartTime(date.getValue());
				} else {
					data.setStartTime(start.getValue(date.getValue()));
					data.setEndTime(end.getValue(date.getValue()));
				}
				data.setData(text.getText());
				DateEvent.fire(this, command, data);
				hide();
			} else {
				if (event.getSource() == cancel) {
					hide();
				} else {
					if (data != null && event.getSource() != null && event.getSource() == delete) {
						DateEvent.fire(this, DateEventActions.REMOVE, data);
						hide();
					} else {
						hide();
					}
				}

			}
		}
	}

	@Override
	public HandlerRegistration addDateEventHandler(DateEventListener<? extends DefaultEventData> handler) {
		return addHandler(handler, DateEvent.getType());
	}

}
