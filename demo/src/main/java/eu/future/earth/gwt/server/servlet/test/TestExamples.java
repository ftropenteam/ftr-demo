package eu.future.earth.gwt.server.servlet.test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import eu.future.earth.client.date.DateNoTimeZone;
import eu.future.earth.client.general.PaymentType;
import eu.future.earth.client.user.Gender;
import eu.future.earth.client.user.User;
import eu.future.earth.server.date.SimpleDateNoTimeZoneFormat;

public class TestExamples {

	public static DateNoTimeZone createDate(String date) {
		SimpleDateNoTimeZoneFormat format = new SimpleDateNoTimeZoneFormat("dd-MM-yyyy");
		try {
			return format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;

		}
	}

	public static User createCustomer() {
		return createCustomer(TestIds.CUSTOMER_ID);
	}

	public static User createCustomer(long id) {
		User user = new User();
		user.setBirthDay(createDate("13-07-1973"));
		user.setGender(Gender.MALE);
		user.setFirstName("Marinus");
		user.setId(id);
		user.setLastName("Koekeboe");
		user.setLoginName("Marinus@kiekeboe-world.eu");
		user.setPassword("23fsdfj4$");
		user.setPincode("2312");
		user.setBsn("23423423423");
		user.setEmail("test@kiekeboe-world.eu");
		user.setLocale("NL");
		user.setPhone("06 2427854");
		user.setLastLoggedIn(new DateNoTimeZone());
		return user;
	}

	public static FinancialSettings createCustomerAdress() {
		return createCustomerAdress(TestIds.CUSTOMER_ID);
	}

	public static FinancialSettings createCustomerAdress(long id) {
		FinancialSettings user = new FinancialSettings();
		user.setId(id);
		user.setIbanBankMandaatSignature(createDate("13-01-2014"));
		user.setIbanBankBicNumber("ABN34");
		user.setHouseNumber("4");
		user.setIbanBankMandaat("mnt23");
		user.setIbanNumber("NL91ABNA0417164300");
		user.setStreet("Erenstein");
		user.setTown("Zeewolde");
		user.setRegistrationNumber(id + "-23423566");
		user.setPostalCode("3456 TE");
		return user;
	}

	public static Invoice createInvoice(boolean generalLines) {

		Invoice data = new Invoice();
		data.setCustomer(createCustomer());
		data.setInvoiceDate(new DateNoTimeZone());
		data.setInvoiceNumber("345");
		data.setMonth(5);
		data.setVat(345);
		data.setYear(2008);
		data.setPaymentType(PaymentType.Sepa);
		GregorianCalendar helper = (GregorianCalendar) GregorianCalendar.getInstance();
		helper.set(Calendar.YEAR, 2);
		helper.set(Calendar.MONTH, 2);

		helper.getTime();
		ArrayList<InvoiceLine> lines = new ArrayList<InvoiceLine>();

		{
			InvoiceLine line = new InvoiceLine();
			line.setQuantity(3);
			line.setPricePerUnit(234);
			line.setDescription("Jan Nouwens hele dagen");
			line.setProductId(ProductTests.DEFAULT_PRODUCT);
			line.setVat(0);
			line.setChildId(4);
			line.calculateTotal();
			lines.add(line);
		}
		{
			InvoiceLine line = new InvoiceLine();
			line.setQuantity(-1);
			line.setPricePerUnit(234);
			line.setDescription("Betsie Nouwens Toeslag");
			line.calculateTotal();
			line.setChildId(4);
			line.setVat(0);
			lines.add(line);
		}
		if (generalLines) {

			InvoiceLine line = new InvoiceLine();
			line.setQuantity(3);
			line.setPricePerUnit(234.5);
			line.setDescription("Extra regel demo geen kind");
			line.calculateTotal();
			line.setVat(0);
			lines.add(line);
		}

		{
			InvoiceLine line = new InvoiceLine();
			line.setQuantity(3);
			line.setPricePerUnit(1234);
			line.setDescription("Betsie Nouwens Hele dagen");
			line.setProductId(ProductTests.DEFAULT_PRODUCT);
			line.calculateTotal();
			line.setChildId(6);
			line.setVat(0);
			lines.add(line);
		}
		{
			InvoiceLine line = new InvoiceLine();
			line.setQuantity(1);
			line.setPricePerUnit(234);
			line.setDescription("Betsie Nouwens Extra dagen");
			line.setProductId(ProductTests.FLEXIBLE_DAYS);
			line.calculateTotal();
			line.setChildId(6);
			line.setVat(0);
			lines.add(line);
		}
		{
			InvoiceLine line = new InvoiceLine();
			line.setQuantity(-1);
			line.setPricePerUnit(234);
			line.setDescription("Betsie Nouwens Toeslag");
			line.setProductId(ProductTests.SPLIT_NO_SUB);
			line.calculateTotal();
			line.setChildId(6);
			line.setVat(0);
			lines.add(line);
		}
		data.setLines(lines);
		data.calculateTotal();
		return data;
	}

}
