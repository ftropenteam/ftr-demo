package eu.future.earth.gwt.client.pages.sepa;

import java.io.Serializable;

import eu.future.earth.sepa.client.credit.SepaPayment;
import eu.future.earth.sepa.client.debit.SepaIncasso;

public class SepaImport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8437833532506652541L;

	private boolean credit = true;

	public SepaImport() {
		super();
	}

	private SepaPayment payments = null;

	private SepaIncasso debitData = null;

	public boolean isCredit() {
		return credit;
	}

	public void setCredit(boolean credit) {
		this.credit = credit;
	}

	public SepaPayment getPayments() {
		return payments;
	}

	public void setpayments(SepaPayment creditData) {
		this.payments = creditData;
	}

	public SepaIncasso getDebitData() {
		return debitData;
	}

	public void setDebitData(SepaIncasso debitData) {
		this.debitData = debitData;
	}

}
