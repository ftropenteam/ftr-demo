package eu.future.earth.gwt.server.servlet;

import eu.duo.dfk.test.SampleEjb;
import eu.future.earth.client.demo.TestInfo;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet(urlPatterns = "/secure/ejb")
public class EjbCallTest extends HttpServlet {
    @EJB
    private SampleEjb sampleEjb = null;

    @Override
    protected void doGet(HttpServletRequest reqest, HttpServletResponse response)
            throws ServletException, IOException {
        TestInfo cl = new TestInfo();
        cl.setName(UUID.randomUUID().toString());
        sampleEjb.register(cl);
        response.getWriter().println("Hello World!" + cl.getName());
    }

}
