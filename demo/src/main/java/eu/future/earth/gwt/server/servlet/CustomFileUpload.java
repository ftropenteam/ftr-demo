package eu.future.earth.gwt.server.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;

import eu.future.earth.client.date.DateNoTimeZone;
import eu.future.earth.file.AttachmentData;
import eu.future.earth.gwt.client.communication.ServletConstants;
import gwtupload.server.UploadAction;
import gwtupload.server.exceptions.UploadActionException;
import gwtupload.shared.UConsts;

public class CustomFileUpload extends UploadAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4032950021671822731L;

	private static AttachmentData getFile(HttpServletRequest request, String fileName) {
		AttachmentData file = (AttachmentData) request.getSession().getAttribute(fileName);
		return file;
	}

	private static Hashtable<String, AttachmentData> getReferenceForPrefix(HttpServletRequest request, String prefix) {
		@SuppressWarnings("unchecked")
		Hashtable<String, AttachmentData> files = (Hashtable<String, AttachmentData>) request.getSession().getAttribute(prefix);
		if (files == null) {
			files = new Hashtable<String, AttachmentData>();
			request.getSession().setAttribute(prefix, files);
		}
		return files;
	}

	public static AttachmentData getFileForPrefix(HttpServletRequest request, String prefix) {
		Hashtable<String, AttachmentData> files = getReferenceForPrefix(request, prefix);
		ArrayList<AttachmentData> res = new ArrayList<AttachmentData>(files.values());
		if (res.isEmpty()) {
			return null;
		} else {
			return res.get(0);
		}
	}

	public static AttachmentData getFileForPrefixAndClear(HttpServletRequest request, String prefix) {
		AttachmentData result = getFileForPrefix(request, prefix);
		clearPrefix(request, prefix);
		return result;
	}

	public static ArrayList<AttachmentData> getFilesForPrefixAndClear(HttpServletRequest request, String prefix) {
		ArrayList<AttachmentData> result = getFilesForPrefix(request, prefix);
		clearPrefix(request, prefix);
		return result;
	}

	public static ArrayList<AttachmentData> getFilesForPrefix(HttpServletRequest request, String prefix) {
		Hashtable<String, AttachmentData> files = getReferenceForPrefix(request, prefix);
		return new ArrayList<AttachmentData>(files.values());
	}

	/**
	 * prefix name contains the files. The file name contains the prefix under which it is stored.
	 * 
	 * @param request
	 * @param prefix
	 * @param fileName
	 * @param newFile
	 */
	public static void addFile(HttpServletRequest request, String prefix, String fileName, AttachmentData newFile) {
		Hashtable<String, AttachmentData> files = getReferenceForPrefix(request, prefix);
		files.put(fileName, newFile);
		request.getSession().setAttribute(fileName, newFile);
	}

	public static void clearPrefix(HttpServletRequest request, String prefix) {
		Hashtable<String, AttachmentData> files = getReferenceForPrefix(request, prefix);
		for (String fileName : files.keySet()) {
			request.getSession().removeAttribute(fileName);
		}
		files.clear();
	}

	public static void removeFile(HttpServletRequest request, String fileName) {
		String prefix = (String) request.getSession().getAttribute(fileName);
		Hashtable<String, AttachmentData> files = getReferenceForPrefix(request, prefix);
		files.remove(fileName);
		request.getSession().removeAttribute(fileName);
	}

	/**
	 * Override executeAction to save the received files in a custom place and delete this items from session.
	 */
	@Override
	public String executeAction(HttpServletRequest request, List<FileItem> sessionFiles) throws UploadActionException {
		String response = "";
		for (FileItem item : sessionFiles) {
			if (false == item.isFormField()) {
				for (String prefix : ServletConstants.FILE_PREFIXEN) {
					if (item.getFieldName().startsWith(prefix)) {
						AttachmentData newFile = new AttachmentData();
						newFile.setModificationDate(new DateNoTimeZone());
						newFile.setFileType(AttachmentData.getFileType(item.getName()));
						newFile.setName(item.getName());
						newFile.setSize(item.getSize());
						newFile.setData(item.get());
						addFile(request, prefix, item.getFieldName(), newFile);
					}
				}

				try {

					// / Send a customized message to the client.
					response += "File saved as " + item.getFieldName();

				} catch (Exception e) {
					throw new UploadActionException(e);
				}
			}
		}

		// / Remove files from session because we have a copy of them
		removeSessionFileItems(request);

		// / Send your customized message to the client.
		return response;
	}

	/**
	 * Get the content of an uploaded file.
	 */
	@Override
	public void getUploadedFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String fieldName = request.getParameter(UConsts.PARAM_SHOW);
		AttachmentData f = getFile(request, fieldName);
		if (f != null) {
			response.setContentType(f.determineContentType());
			ByteArrayInputStream is = new ByteArrayInputStream(f.getData());
			copyFromInputStreamToOutputStream(is, response.getOutputStream());
		} else {
			renderXmlResponse(request, response, XML_ERROR_ITEM_NOT_FOUND);
		}
	}

	/**
	 * Remove a file when the user sends a delete request.
	 */
	@Override
	public void removeItem(HttpServletRequest request, String fieldName) throws UploadActionException {
		removeFile(request, fieldName);
	}
}
