package eu.future.earth.gwt.client.pages.slider;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Composite;

import eu.future.earth.gwt.client.labels.DoubleLabel;
import eu.future.earth.slider.client.DoubleSliderBarVertical;

public class Temparature extends Composite implements HasValueChangeHandlers<Double> {

	private DoubleLabel value = new DoubleLabel();

	private DoubleSliderBarVertical slider = new DoubleSliderBarVertical(35, 42, 37);

	public Temparature() {
		super();
		slider.setTickValue(0.1);
		slider.setMajorTickValue(1.0);
		slider.setLabelSpacing(5);
		AbsolutePanel milkSlider = new AbsolutePanel();
		milkSlider.setPixelSize(360, 450);
		milkSlider.add(slider, 250, 50);
		milkSlider.add(value, 320, 50);
		initWidget(milkSlider);

		addStyleName("temparature");

		slider.addValueChangeHandler(new ValueChangeHandler<Double>() {

			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				value.setValue(event.getValue());
				sendChange();
			}
		});
	}

	protected void sendChange() {
		ValueChangeEvent.fire(this, value.getValue());
	}

	public void reset() {
		value.setValue(37);
	}

	public double getValue() {
		return value.getValue();
	}

	public void setValue(double newValue) {
		if (newValue > 0) {
			slider.setValue(newValue);
			value.setValue(newValue);
		}
	}

	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Double> handler) {
		return super.addHandler(handler, ValueChangeEvent.getType());
	}

}
