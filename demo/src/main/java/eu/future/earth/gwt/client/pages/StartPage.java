package eu.future.earth.gwt.client.pages;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import eu.future.earth.client.demo.TestInfo;
import eu.future.earth.client.general.SectionColor;
import eu.future.earth.gwt.client.callback.FtrCallBackNoError;
import eu.future.earth.gwt.client.communication.UserHelper;
import eu.future.earth.gwt.client.core.ApplicationEventManager;
import eu.future.earth.gwt.client.core.FullPage;
import eu.future.earth.gwt.client.core.Page;
import eu.future.earth.gwt.client.images.UiUtil;
import eu.future.earth.gwt.client.navi.NavigationData;
import eu.future.earth.gwt.client.navi.Navigator;
import eu.future.earth.gwt.client.pages.agenda.AgendaPage.AgendaPageNavigator;
import eu.future.earth.gwt.client.pages.chart.ChartDemoPage.ChartDemoPageNavigator;
import eu.future.earth.gwt.client.pages.horcalendar.HorAgendaPage.HorAgendaPageNavigator;
import eu.future.earth.gwt.client.pages.list.ListDemoPage.ListDemoPageNavigator;
import eu.future.earth.gwt.client.pages.sepa.SepaDemoPage.SepaDemoPageNavigator;
import eu.future.earth.gwt.client.pages.slider.SliderDemoPage.SliderDemoPageNavigator;
import eu.future.earth.gwt.client.panels.DashBoardPanel;
import eu.future.earth.gwt.client.popups.iconselect.IconPopupRenderer;
import eu.future.earth.gwt.client.ui.button.FontAwesomeTouchToggleButton;
import eu.future.earth.gwt.client.ui.button.TouchButton;
import eu.future.earth.gwt.client.ui.button.TouchPopupButton;
import eu.future.earth.gwt.client.ui.button.TouchSelectButton;
import eu.future.earth.gwt.client.ui.event.ClickTouchEvent;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;
import eu.future.earth.gwt.client.ui.navigation.MasonryButton;
import eu.future.earth.gwt.client.ui.popup.PopupRenderer;
import gwt.material.design.client.constants.IconType;

import java.util.ArrayList;
import java.util.List;

public class StartPage extends FullPage {

	public static class BeheerStartScreenRenderer extends Navigator {

		public BeheerStartScreenRenderer() {
			super("Demo", UiUtil.CONSTANTS.startpage());
			add(new AgendaPageNavigator());
			add(new SliderDemoPageNavigator());
			add(new ChartDemoPageNavigator());
			add(new HorAgendaPageNavigator());
			add(new ListDemoPageNavigator());
			add(new SepaDemoPageNavigator());
		}

		public Page createPage() {
			return new StartPage();
		}

	}

	private DashBoardPanel dashboard = new DashBoardPanel(3);

//	private FontAwesomeTouchToggleButton button = new FontAwesomeTouchToggleButton("Toggle");

	private TouchButton buttonI = new TouchButton("Button", new Image(UiUtil.images24.schedule()));

	private TouchPopupButton<String> popop = new TouchPopupButton<String>("Popup no image", new Image(UiUtil.images24.horizontal()), new PopupRenderer<String>() {

		@Override
		public String getId(String newRow) {
			return newRow;
		}

		@Override
		public String getLabel(String item) {
			return item;
		}
	});

	private TouchSelectButton<DemoObject> popopObject = new TouchSelectButton<DemoObject>("Select Image", new Image(UiUtil.images24.horizontal()), new IconPopupRenderer<DemoObject>() {

		@Override
		public String getId(DemoObject newRow) {
			return newRow.getName();
		}

		@Override
		public String getLabel(DemoObject item) {
			return item.getName();
		}

		@Override
		public Image getImage(DemoObject item) {
			return new Image(item.getImage());
		}
	});

	private FlowPanel test = new FlowPanel();

	public Widget getRightActionWidget() {
		return test;
	}

	public StartPage() {
		initWidget(dashboard);
//		test.add(button);
		{
			List<String> choices = new ArrayList<String>();
			choices.add("Wehee 1");
			choices.add("Wehee 2 dffffffffffxxxxxxxxxxxxxxxxxx");
			choices.add("Wehee 3");
			popop.setChoices(choices);
		}

		{
			List<DemoObject> choices = new ArrayList<DemoObject>();
			choices.add(new DemoObject("Calendar", UiUtil.images24.calendar()));
			choices.add(new DemoObject("Chart", UiUtil.images24.chart()));
			choices.add(new DemoObject("Schedule", UiUtil.images24.schedule()));
			choices.add(new DemoObject("Temparature", UiUtil.images24.Temperature()));
			popopObject.setChoices(choices);

			popopObject.addValueChangeHandler(event -> {

				List<DemoObject> choicesOneLess = new ArrayList<DemoObject>();
				choicesOneLess.add(new DemoObject("Calendar", UiUtil.images24.calendar()));
				choicesOneLess.add(new DemoObject("Chart", UiUtil.images24.chart()));
				choicesOneLess.add(new DemoObject("Schedule", UiUtil.images24.schedule()));
				// choices.add(new DemoObject("Temparature", UiUtil.images24.Temperature()));
				popopObject.setChoices(choicesOneLess);

			});

		}

//		button.addClickHandler(event -> buttonI.setEnabled(!buttonI.isEnabled()));
		test.add(popopObject);
		test.add(buttonI);
		test.add(popop);

		buttonI.addClickHandler(event -> ApplicationEventManager.fireInfoEvent("Click" + (event.getSource() == buttonI)));

	}

	private boolean init = true;

//	final IconLabelToggleButton toggle2 = new IconLabelToggleButton("Test", true);

//	final IconLabelToggleButtonWithData<DemoObject> toggle = new IconLabelToggleButtonWithData<DemoObject>("Public", new Image(UiUtil.images128.lock_128()), "Prive", new Image(UiUtil.images128.unlock_128()), ButtonSize.s128, true);

	@Override
	public void onShow(NavigationData data) {
		if (init) {
			dashboard.addButton(getNavigationData(), SepaDemoPageNavigator.NAME, UiUtil.CONSTANTS.sepaDemo(), UiUtil.images128.sepa_128());
//			dashboard.addButton(getNavigationData(), HTmlDemoPageNavigator.NAME, UiUtil.CONSTANTS.htmlDemo(), UiUtil.images128.xhtml_128());
			dashboard.addButton(getNavigationData(), AgendaPageNavigator.NAME, UiUtil.CONSTANTS.calendarDemo(), UiUtil.images128.calendar());
			dashboard.addButton(getNavigationData(), HorAgendaPageNavigator.NAME, UiUtil.CONSTANTS.horizontalDemo(), UiUtil.images128.schedule());
			dashboard.addButton(getNavigationData(), SliderDemoPageNavigator.NAME, UiUtil.CONSTANTS.sliderDemo(), UiUtil.images128.Temperature());
			dashboard.addButton(getNavigationData(), ListDemoPageNavigator.NAME, UiUtil.CONSTANTS.list(), UiUtil.images128.shell_128());
			dashboard.addButton(getNavigationData(), ChartDemoPageNavigator.NAME, UiUtil.CONSTANTS.chartDemo(), UiUtil.images128.chart());
			{
				final MasonryButton clearButton = new MasonryButton(UiUtil.CONSTANTS.pdfFile(), IconType.COMPARE_ARROWS, SectionColor.RED);
				dashboard.addButton(clearButton);
				clearButton.addClickHandler(e -> {

					UserHelper.getDemoService().runBatch(1L, new FtrCallBackNoError<TestInfo>() {
						@Override
						public void onSuccess(TestInfo value) {

						}
					});

				});
			}
//			dashboard.addButton(toggle);
//
//			toggle.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
//
//				@Override
//				public void onValueChange(ValueChangeEvent<Boolean> event) {
//					toggle2.setEnabled(event.getValue());
//				}
//
//			});
//
//			toggle2.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
//
//				@Override
//				public void onValueChange(ValueChangeEvent<Boolean> event) {
//					ApplicationEventManager.fireInfoEvent("Value changed to " + event.getValue());
//				}
//			});
//			dashboard.addButton(toggle2);
			init = false;
		}
	}

}
