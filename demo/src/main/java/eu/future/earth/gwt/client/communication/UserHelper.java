package eu.future.earth.gwt.client.communication;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import eu.future.earth.client.demo.DemoRestClient;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestServiceProxy;

import java.util.ArrayList;
import java.util.List;

public class UserHelper {
	public static String CONTEXT = "/demoio/";
	private static String secureEnviroment = "/demoio/rest/secure";

	private static DemoRestClient demoService = null;

	public static String createUrl(String string) {
		return "/";
	}
	private static List<RestServiceProxy> secureProxys = new ArrayList<>();
	public static DemoRestClient getDemoService() {
		if (demoService == null) {
			demoService = (DemoRestClient) GWT.create(DemoRestClient.class);
			secureProxy((RestServiceProxy) demoService);
		}
		return demoService;
	}


	private static void secureProxy(RestServiceProxy proxy) {
//		proxy.setDispatcher(new SecurityDispatcher());
		final Resource resource = new Resource(secureEnviroment);
		proxy.setResource(resource);
		secureProxys.add(proxy);
	}
}
