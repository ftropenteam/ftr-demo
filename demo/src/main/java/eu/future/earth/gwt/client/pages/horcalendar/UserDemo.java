package eu.future.earth.gwt.client.pages.horcalendar;

import java.io.Serializable;

public class UserDemo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4374949208817867171L;

	private long id = -1;

	private String name = null;

	public UserDemo(long newIs, String newName) {
		super();
		setId(newIs);
		setName(newName);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
