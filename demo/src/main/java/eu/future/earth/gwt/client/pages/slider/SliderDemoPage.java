package eu.future.earth.gwt.client.pages.slider;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import eu.future.earth.gwt.client.core.FullPage;
import eu.future.earth.gwt.client.core.Page;
import eu.future.earth.gwt.client.images.UiUtil;
import eu.future.earth.gwt.client.navi.NavigationData;
import eu.future.earth.gwt.client.navi.Navigator;
import eu.future.earth.gwt.client.pages.DemoObject;
import eu.future.earth.gwt.client.popups.iconselect.IconPopupRenderer;
import eu.future.earth.gwt.client.ui.button.TouchPopupButton;

import java.util.ArrayList;
import java.util.List;

public class SliderDemoPage extends FullPage {

    private TouchPopupButton<DemoObject> popopObject = new TouchPopupButton<DemoObject>("Popup I",
            new Image(UiUtil.images24.horizontal()), new IconPopupRenderer<DemoObject>() {

        @Override
        public String getId(DemoObject newRow) {
            return newRow.getName();
        }

        @Override
        public String getLabel(DemoObject item) {
            return item.getName();
        }

        @Override
        public Image getImage(DemoObject item) {
            return new Image(item.getImage());
        }
    });
    private FlowPanel test = new FlowPanel();
    private DoubleBox value = new DoubleBox();

    public SliderDemoPage() {
        super();

        FlowPanel main = new FlowPanel();

        Temparature demo = new Temparature();

        FlowPanel top = new FlowPanel();
        top.add(main);

        initWidget(top);
        // add First
        main.add(demo);
        demo.addValueChangeHandler(new ValueChangeHandler<Double>() {

            @Override
            public void onValueChange(ValueChangeEvent<Double> event) {
                value.setText(String.valueOf(event.getValue()));
            }
        });
        value.setValue((double) demo.getValue());

        List<DemoObject> choices = new ArrayList<DemoObject>();
        choices.add(new DemoObject("Calendar", UiUtil.images24.calendar()));
        choices.add(new DemoObject("Chart", UiUtil.images24.chart()));
        choices.add(new DemoObject("Schedule", UiUtil.images24.schedule()));
        choices.add(new DemoObject("Temparature", UiUtil.images24.Temperature()));
        popopObject.setChoices(choices);

        popopObject.addValueChangeHandler(new ValueChangeHandler<DemoObject>() {

            @Override
            public void onValueChange(ValueChangeEvent<DemoObject> event) {

            }
        });
        test.add(popopObject);
    }

    public Widget getRightActionWidget() {
        return test;
    }

    @Override
    public void onShow(NavigationData data) {

    }

    public static class SliderDemoPageNavigator extends Navigator {

        public static String NAME = "slider";

        public SliderDemoPageNavigator() {
            super(NAME, UiUtil.CONSTANTS.sliderDemo());
        }

        public Page createPage() {
            return new SliderDemoPage();
        }

    }

}
