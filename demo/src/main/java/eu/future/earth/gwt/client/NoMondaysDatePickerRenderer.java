package eu.future.earth.gwt.client;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.date.picker.DatePickerMonthPanelDayPanel;
import eu.future.earth.gwt.client.date.picker.DatePickerRenderer;

public class NoMondaysDatePickerRenderer implements DatePickerRenderer {

	public boolean isEnabled(Date event, DatePickerMonthPanelDayPanel parent) {
		return isEnabled(event);
	}

	@Override
	public Widget getBottumWidget(Date newDate) {
		return null;
	}

	@Override
	public String getExtraStyle(Date newDate) {
		return null;
	}

	@Override
	public String getTitle(Date newDate) {
		return null;
	}

	@Override
	public boolean isEnabled(Date event) {
		GregorianCalendar helper = new GregorianCalendar();
		helper.setTime(event);
		if(helper.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY){
			return false;
		}
		if(helper.get(Calendar.DAY_OF_MONTH) == 12){
			return false;
		}
		return true;
	}
}
