/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client.pages.agenda;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import eu.future.earth.gwt.client.date.BaseDateRenderer;
import eu.future.earth.gwt.client.date.DateEvent.DateEventActions;
import eu.future.earth.gwt.client.date.DateEventListener;
import eu.future.earth.gwt.client.date.DatePanel;
import eu.future.earth.gwt.client.date.DateUtils;
import eu.future.earth.gwt.client.date.EventPanel;
import eu.future.earth.gwt.client.date.PanelType;
import eu.future.earth.gwt.client.pages.horcalendar.DemoHandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class DefaultStringPanelRenderer extends BaseDateRenderer<DefaultEventData> implements DemoHandler {

	public DefaultStringPanelRenderer() {
		super();
	}

	public void createNewAfterClick(Date currentDate, DateEventListener<? extends DefaultEventData> listener) {
		final DefaultEventData data = new DefaultEventData(System.currentTimeMillis());
		data.setStartTime(currentDate);
		final DefaultStringEventDataDialog dialog = new DefaultStringEventDataDialog(this, data);
		dialog.addDateEventHandler(listener);
		dialog.show();
		dialog.center();
	}

	public void editAfterClick(DefaultEventData data, DateEventListener<? extends DefaultEventData> listener) {
		final DefaultStringEventDataDialog dialog = new DefaultStringEventDataDialog(this, data, DateEventActions.UPDATE);
		dialog.addDateEventHandler(listener);
		dialog.show();
		dialog.center();
	}

	public void createNewAfterClick(Date currentDate, Date endDate, DateEventListener<? extends DefaultEventData> listener) {
		final DefaultEventData data = new DefaultEventData(System.currentTimeMillis());
		data.setStartTime(currentDate);
		data.setEndTime(endDate);
		final DefaultStringEventDataDialog dialog = new DefaultStringEventDataDialog(this, data);
		dialog.addDateEventHandler(listener);
		dialog.show();
		dialog.center();

	}

	public boolean show24HourClock() {
		return true;
	}

	public boolean showIntervalTimes() {
		return false;
	}

	public boolean supportListView() {
		return true;
	}

	public boolean showDay(int dayCode) {
		if(dayCode == Calendar.THURSDAY){
			return false;
		}
		if(dayCode == Calendar.SATURDAY){
			return false;
		}
		if(dayCode == Calendar.SUNDAY){
			return false;
		}
		return true;
	}
	
	@Override
	public boolean supportWeekListView() {
		return true;
	}

	public Widget createPickerPanel(Object newData, int day) {
		return null;
	}

	public boolean supportDayView() {
		return true;
	}

	public boolean supportMonthView() {
		return true;
	}

	public boolean showWholeDayEventView() {
		return true;
	}

	public boolean supportWeekView() {
		return true;
	}

	public boolean enableDragAndDrop() {
		return true;
	}

	public int getEndHour() {
		return 24;
	}

	public int getStartHour() {
		return 0;
	}

	public int showDaysInWeek() {
		return 7;
	}

	public Date getEndTime(DefaultEventData event) {
		final DefaultEventData data = getData(event);
		return data.getEndTime();
	}

	private DefaultEventData getData(DefaultEventData event) {
		if (event instanceof DefaultEventData) {
			return (DefaultEventData) event;
		} else {
			Window.alert("Not the Right type " + event.getClass());
			return null;
		}
	}

	public String getId(DefaultEventData event) {
		final DefaultEventData data = getData(event);
		return data.getIdentifier();
	}

	public Date getStartTime(DefaultEventData event) {
		final DefaultEventData data = getData(event);
		return data.getStartTime();
	}

	public void setEndTime(DefaultEventData event, Date newEnd) {
		final DefaultEventData data = getData(event);
		GWT.log("Set end called " + newEnd, null);
		data.setEndTime(newEnd);

	}

	public void setStartTime(DefaultEventData event, Date newStart) {
		final DefaultEventData data = getData(event);
		data.setStartTime(newStart);
	}

	public boolean isWholeDayEvent(DefaultEventData event) {
		if (event != null) {
			return event.isWholeDay();
		} else {
			Window.alert("Programming Error " + event);
			return true;
		}
	}

	public EventPanel<DefaultEventData> createPanel(DefaultEventData newData, PanelType viewType) {
		final DefaultEventData data = getData(newData);

		if (data.isWholeDay()) {
			DefaultWholeDayField panel = new DefaultWholeDayField(this, newData);
			return panel;
		} else {

			switch (viewType) {
			case MONTH: {
				final DefaultMonthField panel = new DefaultMonthField(this, newData);
				return panel;

			}
			case LIST: {
				final DefaultListField panel = new DefaultListField(this, newData);
				return panel;

			}
			case WEEK: {
				final DefaultDayField panel = new DefaultDayField(this, newData);
				return panel;
			}
			case DAY: {
				final DefaultDayField panel = new DefaultDayField(this, newData);
				return panel;
			}
			default: {
				final DefaultDayField panel = new DefaultDayField(this, newData);
				return panel;
			}
			}
		}
	}

	public boolean useShowMore() {
		return true;
	}

	public int getEventBottomHeight() {
		return 6;
	}

	public int getEventCornerSize() {
		return 0;
	}

	public int getEventMinimumHeight() {
		return 30;
	}

	public int getEventTopHeight() {
		return 18;
	}

	public int getIntervalHeight() {
		return 20;
	}

	public int getIntervalsPerHour() {
		return 4;
	}

	public int getScrollHour() {
		return 9;
	}

	public boolean isDurationAcceptable(int minutes) {
		return minutes >= (60 / getIntervalsPerHour());
	}

	@Override
	public boolean equal(DefaultEventData one, DefaultEventData two) {
		return one.getIdentifier().equals(two.getIdentifier());
	}

	private HashMap<String, DefaultEventData> items = new HashMap<String, DefaultEventData>();

	@Override
	public void getEventsForRange(Date start, Date end, DatePanel<DefaultEventData> caller, boolean reloadData) {
		ArrayList<DefaultEventData> found = new ArrayList<DefaultEventData>();
		Iterator<DefaultEventData> walker = items.values().iterator();
		while (walker.hasNext()) {
			DefaultEventData data = (DefaultEventData) walker.next();
			if ((data.getStartTime().after(start) && data.getStartTime().before(end)) || DateUtils.isSameDay(data.getStartTime(), start) || DateUtils.isSameDay(data.getStartTime(), end)) {
				found.add(data);
			}
		}
		caller.setEvents(found);
	}

//	@Override
//	public void updateEvent(UpdateCallBackHandler<DefaultEventData> callback) {
//		items.put(callback.getData().getIdentifier(), callback.getData());
//		Window.alert("Server Update");
//		callback.succes();
//	}
//
//	@Override
//	public void removeEvent(RemoveCallBackHandler<DefaultEventData> callback) {
//		DefaultEventData data = (DefaultEventData) callback.getData();
//		// Window.alert("Remove" + items.size());
//		items.remove(data.getIdentifier());
//		Window.alert("Server Delete");
//		// Window.alert("Remove" + items.size());
//		callback.succes();
//	}
//
//	@Override
//	public void addEvent(AddCallBackHandler<DefaultEventData> callback) {
//		DefaultEventData data = (DefaultEventData) callback.getData();
//		items.put(data.getIdentifier(), data);
//		Window.alert("Server Add");
//		callback.onSuccess(data);
//	}

	public void addEvent(DefaultEventData example) {
		items.put(example.getIdentifier(), example);

	}

	@Override
	public boolean supportDayListView() {
		return true;
	}

}
