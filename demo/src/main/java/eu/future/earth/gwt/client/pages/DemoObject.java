package eu.future.earth.gwt.client.pages;

import java.io.Serializable;

import com.google.gwt.resources.client.ImageResource;

public class DemoObject implements Serializable {

	/**
	 * 
	 */ 
	private static final long serialVersionUID = 9008088501321203971L;
	private String name = null;
	private ImageResource image = null;

	public DemoObject() {
		super();
	}

	public DemoObject(String name, ImageResource image) {
		super();
		this.setName(name);
		this.setImage(image);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ImageResource getImage() {
		return image;
	}

	public void setImage(ImageResource image) {
		this.image = image;
	}

}
