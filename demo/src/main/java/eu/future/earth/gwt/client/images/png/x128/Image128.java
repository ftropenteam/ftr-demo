package eu.future.earth.gwt.client.images.png.x128;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Image128 extends ClientBundle {

	@Source("calendar_128.png")
	ImageResource calendar();

	@Source("Temperature_128.png")
	ImageResource Temperature();

	@Source("full_128.png")
	ImageResource chart();
	
	@Source("accounting_128.png")
	ImageResource horizontal();

	@Source("schedule_128.png")
	ImageResource schedule();

	ImageResource lock_128();
	
	ImageResource sepa_128();

	ImageResource unlock_128();

	ImageResource xhtml_128();

	ImageResource shell_128();

	ImageResource software_development_stages_128();

	
}
