package eu.future.earth.gwt.client;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.event.shared.UmbrellaException;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.StatusCodeException;

import eu.future.earth.client.exception.CodedCheckedException;
import eu.future.earth.client.exception.CodedException;
import eu.future.earth.client.exception.CodedRuntimeException;
import eu.future.earth.gwt.client.communication.UserHelper;
import eu.future.earth.gwt.client.core.Application;
import eu.future.earth.gwt.client.core.ApplicationEventManager;
import eu.future.earth.gwt.client.event.ErrorEvent;
import eu.future.earth.gwt.client.event.ErrorEventHandler;
import gwt.material.design.client.ui.MaterialToast;

public class ErrorFeedbackHandler implements ErrorEventHandler {

	private static final Logger logger = Logger.getLogger("Kiekeboe");

	/**
	 * Static initializer. Only call this method once during initialization of the Application.
	 */
	public static void init() {
		ApplicationEventManager.instance().addErrorEventHandler(new ErrorFeedbackHandler());
	}

	public ErrorFeedbackHandler() {
		super();
	}

	private native void changeUrl(String newLocale)
	/*-{
		$wnd.location.href = newLocale;
	}-*/
	;

	public final static String IMPORT_FAILED = "ImportFailed";

	public static String convertMessage(String messageCode, String[] args) {

		// if (ServerFeedbackCodes.INVOICE_LINES_PRESENT.equals(messageCode)) {
		// return CrecheIcons.VARIABLE.invoiceLinesPresentFor(args[0]);
		// }
		return messageCode + "+" + args;
	}

	public void onError(ErrorEvent event) {

		final Throwable t = event.getThrowable();
		String message = null;
		if (t instanceof IncompatibleRemoteServiceException) {
			// this client is not compatible with the server; cleanup and refresh the
			message = "Refresh app";
		} else {
			if (event.getThrowable() instanceof CodedCheckedException || event.getThrowable() instanceof CodedRuntimeException) {
				CodedException errorReal = (CodedException) event.getThrowable();
				message = convertMessage(errorReal.getMessageid(), errorReal.getParameters());
			} else {
				if (t instanceof StatusCodeException) {
					StatusCodeException errorReal = (StatusCodeException) t;
					if (errorReal.getStatusCode() == 401) {
						changeUrl(UserHelper.createUrl(""));
					}
					if (errorReal.getStatusCode() == 500) {
						message = "Error on server... ";
					}
				} else {
					if (t instanceof InvocationException) {
						message = "Oops something went wrong...";
					} else {
						if (t instanceof UmbrellaException) {
							message = "Oops something went wrong...";
						} else {
							message = event.getMessage();
							if (message == null || message.length() < 1) {
								logger.log(Level.SEVERE, "Error", t);
							}
						}
					}
				}
			}
		}
		if (message != null && message.length() > 0) {
			// This will leave the feedbackpanel color in the error color even for
			// the next message, so probably create different static calls to show
			// error and info messages.
			MaterialToast.fireToast(event.getMessage(), "error-toast");
		}

	}

}
