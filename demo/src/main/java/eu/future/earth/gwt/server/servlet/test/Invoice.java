package eu.future.earth.gwt.server.servlet.test;

import java.util.ArrayList;
import java.util.List;

import eu.future.earth.client.UniqueObject;
import eu.future.earth.client.date.CalculationPeriod;
import eu.future.earth.client.date.DateNoTimeZone;
import eu.future.earth.client.general.PaymentState;
import eu.future.earth.client.general.PaymentType;
import eu.future.earth.client.server.CalculationObject;
import eu.future.earth.client.user.User;

public class Invoice extends UniqueObject implements CalculationPeriod {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4419065055483058036L;

	private double totalPrice = -1;

	private int year = -1;

	private int month = -1;

	private double vat = 0;

	private PaymentState state = PaymentState.UnPayed;

	private boolean export = false;

	// private boolean clieop = false;

	private PaymentType paymentType = PaymentType.Manual;

	private boolean mail = false;

	private boolean mailed = false;

	private double partPaid = 0;

	private long locationId = -1;

	private DateNoTimeZone syncTime = null;
	private DateNoTimeZone invoiceDate = null;

	private String invoiceNumber = null;

	private User customer = null;

	public DateNoTimeZone getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(DateNoTimeZone invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public DateNoTimeZone getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(DateNoTimeZone syncTime) {
		this.syncTime = syncTime;
	}

	public boolean isPaid() {
		return PaymentState.Payed.equals(state);
	}

	public DateNoTimeZone getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(DateNoTimeZone modificationTime) {
		this.modificationTime = modificationTime;
	}

	public String getSyncId() {
		return syncId;
	}

	public void setSyncId(String syncId) {
		this.syncId = syncId;
	}

	private DateNoTimeZone modificationTime = null;

	private String syncId = null;

	public double getPartPaid() {
		return partPaid;
	}

	public boolean isDeclaredNotCollectable() {
		return PaymentState.NotCollectable.equals(state);
	}

	public void setPartPaid(double partPaid) {
		this.partPaid = partPaid;
	}

	public double getAmountLeft() {
		return getTotalPrice() - getPartPaid();
	}

	/**
	 * This field is a Set that must always contain Strings.
	 * 
	 */
	private java.util.List<InvoiceLine> lines = new ArrayList<InvoiceLine>();

	public void setLines(List<InvoiceLine> newEvents) {
		if (newEvents != null) {
			lines = newEvents;
		} else {
			lines = new ArrayList<InvoiceLine>();
		}
	}

	public void calculateTotal() {
		totalPrice = 0.0;
		vat = 0.0;
		for (InvoiceLine line : lines) {
			totalPrice = totalPrice + line.getTotalPrice();
			vat = vat + ((line.getTotalPrice() / (100 + line.getVat())) * line.getVat());
		}
	}

	public double calculateTotalToPay() {
		double result = 0.0;
		for (InvoiceLine line : lines) {
			result += line.getTotalPrice();
		}
		return result;
	}

	public double getAmountOpent() {
		return totalPrice - partPaid;
	}

	public double getAmountOpentIncludingVat() {
		return totalPrice - partPaid;
	}

	public double getVatOpen() {
		double de = getAmountOpentIncludingVat();
		if (de == 0) {
			return 0;
		}
		return vat * (totalPrice / de);
	}

	public int getFromYear() {
		return fromYear;
	}

	public void setFromYear(int fromYear) {
		this.fromYear = fromYear;
	}

	public int getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(int fromMonth) {
		this.fromMonth = fromMonth;
	}

	private int fromYear = -1;

	private int fromMonth = -1;

	@Override
	public int getPeriod() {
		return getMonth();
	}

	public void addLine(InvoiceLine newEvent) {
		lines.add(newEvent);
	}

	public java.util.List<InvoiceLine> getLines() {
		return lines;
	}

	/**
	 * @return the totalPrice
	 */
	public double getTotalPrice() {
		return totalPrice;
	}

	public boolean hasValue() {
		return CalculationObject.hasValue(totalPrice);
	}

	public boolean hasLines() {
		return !lines.isEmpty();
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the year
	 */
	@Override
	public int getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the vat
	 */
	public double getVat() {
		return vat;
	}

	/**
	 * @param vat
	 *            the vat to set
	 */
	public void setVat(double vat) {
		this.vat = vat;
	}

	public boolean isExport() {
		return export;
	}

	public void setExport(boolean export) {
		this.export = export;
	}

	public boolean isMail() {
		return mail;
	}

	public void setMail(boolean mail) {
		this.mail = mail;
	}

	public boolean isMailed() {
		return mailed;
	}

	public void setMailed(boolean mailed) {
		this.mailed = mailed;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public PaymentState getState() {
		return state;
	}

	public void setState(PaymentState state) {
		this.state = state;
	}

	@Override
	public int compareTo(CalculationPeriod o) {
		return CalculationObject.comparePeriod(this, o);
	}

}
