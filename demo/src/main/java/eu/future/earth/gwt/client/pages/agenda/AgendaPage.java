package eu.future.earth.gwt.client.pages.agenda;

import eu.future.earth.gwt.client.core.FullPage;
import eu.future.earth.gwt.client.core.Page;
import eu.future.earth.gwt.client.images.UiUtil;
import eu.future.earth.gwt.client.navi.NavigationData;
import eu.future.earth.gwt.client.navi.Navigator;

public class AgendaPage extends FullPage {

	public static class AgendaPageNavigator extends Navigator {

		public static String NAME = "agenda";

		public AgendaPageNavigator() {
			super(NAME, UiUtil.CONSTANTS.calendarDemo());
		}

		public Page createPage() {
			return new AgendaPage();
		}

	}

	private OverviewPanelDemo dashboard = new OverviewPanelDemo();

	public AgendaPage() {
		initWidget(dashboard);
	}

	@Override
	public void onShow(NavigationData data) {

	}

	@Override
	public void setHeight(final int newHeight) {
		dashboard.setHeight(newHeight);
	}
}
