/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client.pages.horcalendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.date.DateEvent;
import eu.future.earth.gwt.client.date.DateEvent.DateEventActions;
import eu.future.earth.gwt.client.date.horizontal.HorizontalDateRenderer;
import eu.future.earth.gwt.client.date.horizontal.HorizontalDayField;
import eu.future.earth.gwt.client.images.DemoCss;
import eu.future.earth.gwt.client.pages.agenda.DefaultEventData;

public class DefaultHorizontalDayField extends HorizontalDayField<DefaultEventData, UserDemo> implements ClickHandler {

	private Label description = new Label();

	private DateTimeFormat format = DateTimeFormat.getFormat("HH:mm");

	private Label title = new Label("x");

	public DefaultHorizontalDayField(HorizontalDateRenderer<DefaultEventData, UserDemo> renderer, DefaultEventData data) {
		super(renderer, data);
		description.addClickHandler(this);
		setWidgetInsizeResize(title, description);
		description.setWordWrap(false);
		final DefaultEventData theData = super.getValue();
		if (theData != null) {
			helper.setTime(theData.getStartTime());
			if (helper.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
				super.setEventStyleName(DemoCss.EVENT_PANEL_MONDAY);
			}
			description.setText(theData.getData());
			repaintTime();
		}
	}

	@Override
	public void repaintTime() {
		final DefaultEventData real = super.getValue();
		if (real.getEndTime() == null) {
			super.setTitle(format.format(real.getStartTime()));
		} else {
			super.setTitle(format.format(real.getStartTime()) + "-" + format.format(real.getEndTime()));
		}

	}

	public Widget getClickableItem() {
		return description;
	}

	GregorianCalendar helper = new GregorianCalendar();

	public Widget getDraggableItem() {
		return title;
	}

	public void onClick(ClickEvent widget) {
		DateEvent.fire(this, DateEventActions.EDIT, getValue());
	}

}
