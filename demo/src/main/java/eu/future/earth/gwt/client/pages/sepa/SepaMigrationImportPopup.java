package eu.future.earth.gwt.client.pages.sepa;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;

import eu.future.earth.gwt.client.callback.AsyncCallBackNoError;
import eu.future.earth.gwt.client.communication.UserHelper;
import eu.future.earth.gwt.client.file.FileEditAndUpload;
import eu.future.earth.gwt.client.file.FileEvent;
import eu.future.earth.gwt.client.file.FileEventListener;
import eu.future.earth.gwt.client.language.EeResources;
import eu.future.earth.gwt.client.ui.UiHelper;
import eu.future.earth.gwt.client.ui.button.CleanDialog;
import eu.future.earth.gwt.client.ui.panels.LabelFieldPanel;
import eu.future.earth.gwt.client.ui.panels.PanelWithMainAndButtonPanel;
import eu.future.earth.sepa.client.SepaUi;
import gwt.material.design.client.constants.Color;
import gwt.material.design.client.ui.MaterialButton;

public class SepaMigrationImportPopup extends CleanDialog implements ClickHandler, FileEventListener {

	private MaterialButton sendButton = null;

	private MaterialButton cancelButton = null;

	private LabelFieldPanel custom = new LabelFieldPanel();

	private SepaDemoPage parent = null;

	private FileEditAndUpload<NoStoreUpload> image = new FileEditAndUpload<NoStoreUpload>(new NoStoreUploadRenderer());

	private CheckBox credit = new CheckBox();

	public SepaMigrationImportPopup(SepaDemoPage newParent) {
		super(true);
		parent = newParent;
		setText("Importeer Sepa");
		sendButton = UiHelper.newMaterialButton(EeResources.EE_TEXTS.ok());
		cancelButton = UiHelper.newMaterialButton(EeResources.EE_TEXTS.cancel());
		sendButton.addClickHandler(this);
		sendButton.setBackgroundColor(Color.GREEN);
		cancelButton.addClickHandler(this);
		image.addFileEventHandler(this);
		custom.addRow(SepaUi.CONSTANTS.sepaDirectDebit(), credit);
		custom.addRow(EeResources.EE_TEXTS.select(), image);
		PanelWithMainAndButtonPanel main = new PanelWithMainAndButtonPanel(custom);
		main.addButton(sendButton);
		main.addButton(cancelButton);
		setWidget(main);
		sendButton.setEnabled(false);
		image.reset();
	}

	public void handleFileEvent(FileEvent newEvent) {
		switch (newEvent.getCommand()) {
		case UPLOAD_STARTED: {
			sendButton.setEnabled(false);
			break;
		}
		case UPLOAD_DONE: {
			sendButton.setEnabled(true);
			break;
		}
		default:
			break;
		}

	}

	public void onClick(ClickEvent sender) {
		if (sender.getSource() == cancelButton) {
			hide();
		} else {
			if (sender.getSource() == sendButton) {
//				final AsyncCallback<SepaImport> callback = new AsyncCallBackNoError<SepaImport>() {
//					public void onSuccess(SepaImport result) {
//						parent.setImport(result);
//						hide();
//					}
//
//				};
//				UserHelper.getDemoService().importSepa(credit.getValue(), callback);
			}
		}
	}

}
