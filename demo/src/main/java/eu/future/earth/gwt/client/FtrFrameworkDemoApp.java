/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;

import eu.future.earth.gwt.client.core.Application;
import eu.future.earth.gwt.client.core.InfoFeedbackHandler;
import eu.future.earth.gwt.client.core.Startup;
import eu.future.earth.gwt.client.core.StartupApplication;
import eu.future.earth.gwt.client.navi.NavigationData;
import eu.future.earth.gwt.client.navi.NavigationManager;
import eu.future.earth.gwt.client.navi.Navigator;
import eu.future.earth.gwt.client.pages.StartPage;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FtrFrameworkDemoApp implements EntryPoint, Startup {

	private String navigateTo = null;

	public void prepare() {

	}

	private static final Logger logger = Logger.getLogger("Demo");

	public void onModuleLoad() {
		StartupApplication.instance().start(this, "Loading...");
	}

	private Anchor emailAnchor = new Anchor("----");

	/**
	 * This is the entry point method.
	 */
	public void init() {

		try {
			navigateTo = Window.Location.getParameter("navigateTo");
		} catch (Exception e) {
			logger.log(Level.WARNING, "No Parameter", e);
		}

//		TrailPanel newTrail = new TrailPanel();
//		Application.getTopPanel().addWidget(newTrail);

//		HelpHandler helpLink = new HelpHandler();
//		Application.getTopUserPanel().addWidget(helpLink);
//		NavigationManager.addNavigationEventHandler(helpLink);

		ErrorFeedbackHandler.init();
		InfoFeedbackHandler.init();
		loadInfoFromServer();
	}

	private Navigator root = null;

	public void setInitReady() {
		root = new StartPage.BeheerStartScreenRenderer();
		NavigationManager.getInstance().registerRoot(root);
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				String current = History.getToken();
				if (current != null && current.length() > 0) {
					NavigationManager.fireOnSelected(NavigationData.decodeHistoryToken(current));
				} else {
					if (navigateTo != null) {
						NavigationManager.fireOnSelected(NavigationData.decodeHistoryToken(navigateTo));
					} else {
						NavigationManager.fireOnSelected(NavigationData.decodeHistoryToken(root.getName()));
					}
				}

			}

		});
	}

	public void loadInfoFromServer() {

		ScreenInfo userData = new ScreenInfo();
//		Application.getTopRightPanel().add(emailAnchor);

		emailAnchor.setText(userData.getLoginName());
		emailAnchor.setTitle(userData.getPrintableName() + "(" + userData.getEmail() + ")");
		emailAnchor.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				showAccountOptions();

			}

		});

		setInitReady();

	}

	private void showAccountOptions() {

	}

}
