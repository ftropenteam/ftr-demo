package eu.future.earth.gwt.server.servlet.test;

public interface TestIds {
	int KDV_ZONNETJE = 2;

	int BSO_DE_BENGELS = 4;

	int CUSTOMER_ID = 231;

	int CUSTOMER_PARTNER_ID = 2311;

	int SCHOOL_TOERMALIJN = 3;

	int NANNY_ID = 923;

	String ZONNETJE_LRK_BSO = "720500333";

	String ZONNETJE_LRK_KDV = "277611878";
}
