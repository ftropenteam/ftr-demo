package eu.future.earth.gwt.client.images;

import com.google.gwt.core.client.GWT;

import eu.future.earth.gwt.client.images.png.x128.Image128;
import eu.future.earth.gwt.client.images.png.x24.Images24;

public class UiUtil {

	public static final HtmlInterface HTML_FILES = GWT.create(HtmlInterface.class);

	
	public static final DemoConstants CONSTANTS = GWT.create(DemoConstants.class);

	public static final Images24 images24 = GWT.create(Images24.class);

	public static final Image128 images128 = GWT.create(Image128.class);

}
