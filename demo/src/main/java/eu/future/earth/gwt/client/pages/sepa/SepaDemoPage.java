package eu.future.earth.gwt.client.pages.sepa;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import eu.future.earth.gwt.client.UrlBuilder;
import eu.future.earth.gwt.client.callback.AsyncCallBackNoError;
import eu.future.earth.gwt.client.communication.UserHelper;
import eu.future.earth.gwt.client.core.FullPage;
import eu.future.earth.gwt.client.core.Page;
import eu.future.earth.gwt.client.images.UiUtil;
import eu.future.earth.gwt.client.labels.Filler;
import eu.future.earth.gwt.client.navi.NavigationData;
import eu.future.earth.gwt.client.navi.Navigator;
import eu.future.earth.gwt.client.ui.UiHelper;
import eu.future.earth.gwt.client.ui.button.TouchButton;
import eu.future.earth.gwt.client.ui.event.ClickTouchEvent;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;
import eu.future.earth.sepa.client.SepaPanel;
import eu.future.earth.sepa.client.SepaUi;
import eu.future.earth.sepa.client.credit.SepaPaymentPanel;
import eu.future.earth.sepa.client.debit.SepaIncassoPanel;

import java.util.logging.Logger;

public class SepaDemoPage extends FullPage {

    private static final Logger logger = Logger.getLogger("Kiekeboe");
    private FlowPanel test = new FlowPanel();

    private TouchButton uploadButton = UiHelper.createButton(SepaUi.CONSTANTS.importSepaFile());

    private TouchButton downloadButton = UiHelper.createButton(SepaUi.CONSTANTS.downloadSepaFile());
    private FlowPanel main = new FlowPanel();
    private SepaPaymentPanel payments = new SepaPaymentPanel();
    private SepaIncassoPanel debit = new SepaIncassoPanel();
    private HorizontalPanel top = new HorizontalPanel();
    private SepaPanel current = null;

    public SepaDemoPage() {
        initWidget(main);
        test.add(uploadButton);
        test.add(downloadButton);
        uploadButton.addClickHandler(new ClickTouchHandler() {

            @Override
            public void onClick(ClickTouchEvent event) {
                importFile();

            }
        });

        main.clear();
        main.add(top);
        main.add(new Filler());

        downloadButton.addClickHandler(new ClickTouchHandler() {

            @Override
            public void onClick(ClickTouchEvent event) {
                createPdf();
            }
        });
    }

    public Widget getRightActionWidget() {
        return test;
    }

    protected void createPdf() {
//        if (current instanceof SepaIncassoPanel) {
//            UserHelper.getDemoService().setSepaForDownload(debit.getValue(), new AsyncCallBackNoError<Void>() {
//
//                @Override
//                public void onSuccess(Void result) {
//                    UrlBuilder url = new UrlBuilder(UserHelper.CONTEXT + "/sepa/test");
//                    Window.open(url.createUrl(), "_blank", "");
//                }
//            });
//        } else {
//            UserHelper.getDemoService().setSepaForDownload(payments.getValue(), new AsyncCallBackNoError<Void>() {
//
//                @Override
//                public void onSuccess(Void result) {
//                    UrlBuilder url = new UrlBuilder(UserHelper.CONTEXT + "/sepa/test");
//                    Window.open(url.createUrl(), "_blank", "");
//                }
//            });
//        }
    }

    protected void importFile() {
        SepaMigrationImportPopup popup = new SepaMigrationImportPopup(this);
        popup.center();
    }

    @Override
    public void onShow(NavigationData data) {

    }

    public void setImport(SepaImport result) {
        if (!result.isCredit()) {
            current = payments;
            logger.info("Setting payment");
            payments.setData(result.getPayments());
        } else {
            current = debit;
            logger.info("setting credit");
            debit.setData(result.getDebitData());
        }
        main.clear();
        main.add(top);
        main.add(current);
    }

    public static class SepaDemoPageNavigator extends Navigator {

        public static String NAME = "sepademo";

        public SepaDemoPageNavigator() {
            super(NAME, UiUtil.CONSTANTS.sepaDemo());
        }

        public Page createPage() {
            return new SepaDemoPage();
        }

    }

}
