package eu.future.earth.gwt.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.future.earth.sepa.client.credit.SepaPayment;
import eu.future.earth.sepa.client.debit.SepaIncasso;
import eu.future.earth.sepa.server.SepaIncassoAdvancedWriter;
import eu.future.earth.sepa.server.SepaPaymentsWriter;

/**
 * Servlet implementation class SaucerServlet
 */
public class SepaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SepaServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object html = request.getSession().getAttribute("sepa");

		if (html instanceof SepaIncasso) {
			SepaIncasso real = (SepaIncasso) html;
			setFileName(response, "Modified-" + real.getMessageId() + ".xml", "application/txt");
			SepaIncassoAdvancedWriter writer = new SepaIncassoAdvancedWriter();
			writer.setData(real);
			writer.writeToStream(response.getOutputStream());
		} else {
			SepaPayment real = (SepaPayment) html;
			setFileName(response, "Modified-" + real.getMessageId() + ".xml", "application/txt");
			SepaPaymentsWriter writer = new SepaPaymentsWriter();
			writer.setData(real);
			writer.writeToStream(response.getOutputStream());
		}

	}

	public static void setFileName(HttpServletResponse response, String name, String type) {
		response.setContentType(type);
		response.addHeader("Content-Disposition", "filename=\"" + name + "\"");
	}

}
