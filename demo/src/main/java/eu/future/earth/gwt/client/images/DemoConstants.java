package eu.future.earth.gwt.client.images;

import com.google.gwt.i18n.client.Constants;

public interface DemoConstants extends Constants {

	@DefaultStringValue("Start Page")
	String startpage();

	@DefaultStringValue("Agenda")
	String calendarDemo();

	@DefaultStringValue("Slider")
	String sliderDemo();

	@DefaultStringValue("Horizontal")
	String horizontalDemo();
	
	@DefaultStringValue("Chart Demo")
	String chartDemo();

	@DefaultStringValue("List Demo")
	String list();

	@DefaultStringValue("Html Editor")
	String htmlDemo();

	@DefaultStringValue("Sepa demo")
	String sepaDemo();

	@DefaultStringValue("Import file")
	String importFile();

	@DefaultStringValue("Pdf file")
	String pdfFile();

	@DefaultStringValue("Download Sepa File")
	String downloadSepaFile();
	
}
