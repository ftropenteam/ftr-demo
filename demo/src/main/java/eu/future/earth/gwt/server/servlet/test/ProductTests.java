package eu.future.earth.gwt.server.servlet.test;

public interface ProductTests {
	public static final int DEFAULT_PRODUCT = 3;

	public static final int FLEXIBLE_DAYS = 4;

	public static final int SPLIT_NO_SUB = 5;
	
	public static final int SPLIT_SUB = 6;
}
