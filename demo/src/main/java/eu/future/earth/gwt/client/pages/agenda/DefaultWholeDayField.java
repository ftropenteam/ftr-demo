/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client.pages.agenda;

import java.util.Calendar;
import java.util.GregorianCalendar;

import eu.future.earth.gwt.client.date.AbstractWholeDayField;
import eu.future.earth.gwt.client.date.DateRenderer;
import eu.future.earth.gwt.client.images.DemoCss;

public class DefaultWholeDayField extends AbstractWholeDayField<DefaultEventData> {

	public DefaultWholeDayField(DateRenderer<DefaultEventData> renderer, DefaultEventData data) {
		super(renderer, data);
		helper.setTime(data.getStartTime());
		if (helper.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
			super.setEventStyleName(DemoCss.EVENT_HEADER_MONDAY);
		}
		setTitle(data.getData());
	}

	public int getContentHeight() {
		return contentHeight;
	}

	private int contentHeight = 15;

	public void setContentHeight(int contentHeight) {
		this.contentHeight = contentHeight;
	}

	GregorianCalendar helper = new GregorianCalendar();

	@Override
	public void repaintTime() {
		// TODO Auto-generated method stub

	}

}
