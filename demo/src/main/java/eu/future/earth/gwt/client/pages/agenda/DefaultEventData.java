/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client.pages.agenda;

import java.io.Serializable;
import java.util.Date;

public class DefaultEventData implements Serializable {

	private static final long serialVersionUID = -6586564827669185408L;

	private Date startTime = null;
 
	private Date endTime = null;

	private String data = null;

	private String id = null; 

	private long masterId = -1;

	public DefaultEventData(long newId) {
		super();
		id = String.valueOf(newId);
		masterId = newId;
	}

	private boolean wholeDay = false;

	public boolean isWholeDay() {
		return wholeDay;
	}

	public void setWholeDay(boolean wholeDay) {
		this.wholeDay = wholeDay;
	}

	public String getData() {
		return data;
	}

	public Date getEndTime() {
		return endTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Returns a string representation of the object.
	 * 
	 * @return a string representation of the object.
	 * @todo Implement this java.lang.Object method
	 */
	public String toString() {
		return getStartTime() + "-" + getEndTime() + " data = " + getData();
	}

	/**
	 * This identifier identifes the evnt in the calander. All updates and such rely on an unique id to handle updates
	 * correctly. In a production like situation we would recommend using the key of the record.
	 * 
	 * @return String - And time based identifier
	 */
	public String getIdentifier() {
		return id;
	}

	public long getMasterId() {
		return masterId;
	}

	public void setMasterId(long masterId) {
		this.masterId = masterId;
		id = String.valueOf(masterId);
	}
}
