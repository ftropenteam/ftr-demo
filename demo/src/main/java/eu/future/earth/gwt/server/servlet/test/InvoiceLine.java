package eu.future.earth.gwt.server.servlet.test;

import javax.xml.bind.annotation.XmlAttribute;

import eu.future.earth.client.date.CalculationPeriod;
import eu.future.earth.client.general.FinancialUtils;
import eu.future.earth.client.server.CalculationObject;

public class InvoiceLine implements CalculationPeriod {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8468332918275602631L;

	@XmlAttribute
	private long id = -1;

	private double quantity = 0;

	private long productId = -1;

	private double pricePerUnit = -1;

	private double totalPrice = -1;

	private String description = null;

	private double vat = 0;

	private boolean generated = true;

	private long childId = -1;

	private int year = -1;

	private int month = -1;

	private double invoiced = -1;

	private double real = -1;

	private long referencedItemId = -1;

	public void calculateTotal() {
		totalPrice = FinancialUtils.roundValue(quantity * pricePerUnit);
	}

	@Override
	public int compareTo(final CalculationPeriod o) {
		return CalculationObject.comparePeriod(this, o);
	}

	public double getAmountVat() {
		return FinancialUtils.roundValue(totalPrice * (vat / 100));
	}

	public long getChildId() {
		return childId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	public double getInvoiced() {
		return invoiced;
	}

	public int getMonth() {
		return month;
	}

	@Override
	public int getPeriod() {
		return month;
	}

	/**
	 * @return the pricePerUnit
	 */
	public double getPricePerUnit() {
		return pricePerUnit;
	}

	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}

	/**
	 * @return the quantity
	 */
	public double getQuantity() {
		return quantity;
	}

	public double getReal() {
		return real;
	}

	public long getReferencedItemId() {
		return referencedItemId;
	}

	public double getTotalAmountIncludingVat() {
		return totalPrice + getAmountVat();
	}

	/**
	 * @return the totalPrice
	 */
	public double getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @return the vat
	 */
	public double getVat() {
		return vat;
	}

	@Override
	public int getYear() {
		return year;
	}

	public boolean isGenerated() {
		return generated;
	}

	public boolean isNew() {
		return id == -1;
	}

	public void setChildId(final long childId) {
		this.childId = childId;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	public void setGenerated(final boolean generated) {
		this.generated = generated;
	}

	public final void setId(final long id) {
		this.id = id;
	}

	public void setInvoiced(final double invoiced) {
		this.invoiced = invoiced;
	}

	public void setMonth(final int month) {
		this.month = month;
	}

	/**
	 * @param pricePerUnit
	 *            the pricePerUnit to set
	 */
	public void setPricePerUnit(final double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	/**
	 * @param pricePerUnit
	 *            the pricePerUnit to set
	 */
	public void setPricePerUnitDirect(final double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(final long productId) {
		this.productId = productId;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(final double quantity) {
		this.quantity = quantity;
	}

	public void setReal(final double real) {
		this.real = real;
	}

	public void setReferencedItemId(final long referencedItemId) {
		this.referencedItemId = referencedItemId;
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(final double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @param vat
	 *            the vat to set
	 */
	public void setVat(final double vat) {
		this.vat = vat;
	}

	public void setYear(final int year) {
		this.year = year;
	}

}
