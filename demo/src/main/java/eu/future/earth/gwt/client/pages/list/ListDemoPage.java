package eu.future.earth.gwt.client.pages.list;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import eu.future.earth.gwt.client.core.FullPage;
import eu.future.earth.gwt.client.core.Page;
import eu.future.earth.gwt.client.images.UiUtil;
import eu.future.earth.gwt.client.navi.NavigationData;
import eu.future.earth.gwt.client.navi.Navigator;
import eu.future.earth.gwt.client.pages.DemoObject;
import eu.future.earth.gwt.client.panels.togglelist.ListToggleButton;
import eu.future.earth.gwt.client.panels.togglelist.ListTogglePanel;
import eu.future.earth.gwt.client.panels.togglelist.ListToggleRenderer;
import eu.future.earth.gwt.client.popups.iconselect.IconPopupRenderer;
import eu.future.earth.gwt.client.popups.select.TouchSelectButtonAndPopupPanel;
import eu.future.earth.gwt.client.ui.ButtonSize;

import java.util.ArrayList;
import java.util.List;

public class ListDemoPage extends FullPage {

    private final ListTogglePanel<DemoObject> instructions = new ListTogglePanel<DemoObject>(new ListToggleRenderer<DemoObject>() {

        @Override
        public String getId(DemoObject newRow) {
            return String.valueOf(newRow.getName());
        }

        @Override
        public ListToggleButton<DemoObject> createToggleButton(DemoObject value) {
            final ListToggleButton<DemoObject> result = new ListToggleButton<DemoObject>(value.getName(), value.getImage(), value);
            if (value.getName().equalsIgnoreCase("Calendar")) {
                result.addWidgetToPanel(new HTML("Richt panel <br />1<br />1<br />1<br />1<br />1<br />1<br />1<br />1<br />1<br />1<br />1" + value.getName()));
            } else {
                result.addWidgetToPanel(new HTML("Richt panel " + value.getName()));
            }
            return result;
        }
    });
    private TouchSelectButtonAndPopupPanel<DemoObject> popop = new TouchSelectButtonAndPopupPanel<DemoObject>("Popup I", new IconPopupRenderer<DemoObject>() {

        @Override
        public String getId(DemoObject newRow) {
            return newRow.getName();
        }

        @Override
        public String getLabel(DemoObject item) {
            return item.getName();
        }

        @Override
        public Image getImage(DemoObject item) {
            return new Image(item.getImage());
        }
    }, true, true);
    private FlowPanel test = new FlowPanel();

    public ListDemoPage() {
        super();
        test.add(popop);

        List<DemoObject> choices = new ArrayList<DemoObject>();
        choices.add(new DemoObject("Calendarsdfs dsfsdfsdf sdfsdf", UiUtil.images24.calendar()));
        for (int i = 0; i < 5; i++) {

            choices.add(new DemoObject("Chart " + i, UiUtil.images24.chart()));
            choices.add(new DemoObject("Schedule" + i, UiUtil.images24.schedule()));
            choices.add(new DemoObject("Temparature" + i, UiUtil.images24.Temperature()));
        }
        popop.setChoices(choices);

        initWidget(instructions);
        instructions.addValue(new DemoObject("Calendar", UiUtil.images128.calendar()));
        instructions.addValue(new DemoObject("Chart", UiUtil.images128.chart()));
        instructions.addValue(new DemoObject("Schedule", UiUtil.images128.schedule()));
        instructions.addValue(new DemoObject("Temparature", UiUtil.images128.Temperature()));

    }

    public Widget getRightActionWidget() {
        return test;
    }

    @Override
    public void onShow(NavigationData data) {

    }

    public static class ListDemoPageNavigator extends Navigator {

        public static String NAME = "list";

        public ListDemoPageNavigator() {
            super(NAME, UiUtil.CONSTANTS.list());
        }

        public Page createPage() {
            return new ListDemoPage();
        }

    }

}
