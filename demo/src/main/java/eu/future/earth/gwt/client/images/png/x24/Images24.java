package eu.future.earth.gwt.client.images.png.x24;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Images24 extends ClientBundle {

	@Source("home_24.png")
	ImageResource startImage();

	@Source("calendar_24.png")
	ImageResource calendar();
	
	@Source("Temperature_24.png") 
	ImageResource Temperature();

	@Source("accounting_24.png")
	ImageResource horizontal();
	
	@Source("full_24.png") 
	ImageResource chart();
	
	@Source("schedule_24.png")
	ImageResource schedule();

	ImageResource sepa_24();
	
	ImageResource xhtml_24();

	ImageResource shell_24();
	
	
}
