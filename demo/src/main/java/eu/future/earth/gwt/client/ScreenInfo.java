package eu.future.earth.gwt.client;

import java.io.Serializable;

public class ScreenInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2377929628555224328L;

	public ScreenInfo() {
		super();
	}

	public String getEmail() {
		return "demo@future-earth.eu";
	}

	public String getPrintableName() {
		return "Demo User";
	}

	public String getLoginName() {
		return "loginName";
	}

}
