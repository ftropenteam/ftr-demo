package eu.future.earth.gwt.client.communication;

public interface ServletConstants {

	String IMPORT_UPLOAD = "import_file_upload_";

	String[] FILE_PREFIXEN = {
		IMPORT_UPLOAD
	};

}
