package eu.future.earth.gwt.client.pages.horcalendar;

import eu.future.earth.gwt.client.pages.agenda.DefaultEventData;

public interface DemoHandler {

	void addEvent(DefaultEventData example); 

}
