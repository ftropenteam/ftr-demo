package eu.future.earth.gwt.client.pages.horcalendar;

import eu.future.earth.gwt.client.core.FullPage;
import eu.future.earth.gwt.client.core.Page;
import eu.future.earth.gwt.client.images.UiUtil;
import eu.future.earth.gwt.client.navi.NavigationData;
import eu.future.earth.gwt.client.navi.Navigator;

public class HorAgendaPage extends FullPage {

    private HorizontalDemo dashboard = new HorizontalDemo();

    public HorAgendaPage() {
        initWidget(dashboard);
    }

    @Override
    public void onShow(NavigationData data) {

    }

    public static class HorAgendaPageNavigator extends Navigator {

        public static String NAME = "horagenda";

        public HorAgendaPageNavigator() {
            super(NAME, UiUtil.CONSTANTS.horizontalDemo());
        }

        public Page createPage() {
            return new HorAgendaPage();
        }

    }

}
