package eu.duo.dfk.test;

import eu.future.earth.client.Unique;
import eu.future.earth.client.change.ChangeLogObject;
import eu.future.earth.client.codes.PictureCodes;
import eu.future.earth.client.communication.MailProtocol;
import eu.future.earth.client.communication.MailSecurity;
import eu.future.earth.client.date.CalendarNoTimeZone;
import eu.future.earth.client.date.FtrLocalDate;
import eu.future.earth.client.picture.PictureId;
import eu.future.earth.client.server.DateHelpers;
import eu.future.earth.client.user.Gender;
import eu.future.earth.client.user.SecurityPrincipalNoRoles;
import eu.future.earth.client.user.UserInfo;
import eu.future.earth.client.user.UserInterface;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import java.util.Calendar;

@Entity
@Table(name = "EE_USERS")
@NamedQueries({
        @NamedQuery(name = "UserEntity.FindById", query = "SELECT a FROM UserEntity a WHERE a.id = :id"),

        @NamedQuery(name = "UserEntity.FindByOrganisation", query = "SELECT a FROM UserEntity a WHERE a.organisationId = :organisationId"),

        @NamedQuery(name = "UserEntity.FindByLogin", query = "SELECT a FROM UserEntity a WHERE LOWER(a.loginName) = LOWER(:loginName)"),

        @NamedQuery(name = "UserEntity.FindByToken", query = "SELECT a FROM UserEntity a WHERE a.secToken = :secToken"),

        @NamedQuery(name = "UserEntity.FindByEmail", query = "SELECT a FROM UserEntity a WHERE LOWER(a.email) LIKE LOWER(:email)"),

        @NamedQuery(name = "UserEntity.FindByName", query = "SELECT a FROM UserEntity a WHERE a.lastName LIKE :lastName AND a.organisationId = :organisationId"),
        @NamedQuery(name = "UserEntity.FindByFirstName", query = "SELECT a FROM UserEntity a WHERE a.firstName LIKE :firstName AND a.organisationId = :organisationId"),
        @NamedQuery(name = "UserEntity.FindAll", query = "SELECT a FROM UserEntity a WHERE a.organisationId = :organisationId")
})
public class UserEntity implements java.io.Serializable, ChangeLogObject, Unique, PictureId, UserInterface, SecurityPrincipalNoRoles {

    /**
     *
     */
    private static final long serialVersionUID = 5177248447324018553L;


    @TableGenerator(name = "userIdGen", table = "EE_SEQUENCE_GENERATOR", pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", pkColumnValue = "USER_ID", initialValue = 203, allocationSize = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "userIdGen")
    @Column(name = "USER_ID", nullable = false)
    private long id = -1;

    @Column(name = "SEC_TOKEN")
    private String secToken = null;

    @Column(name = "FIRST_NAME")
    private String firstName = null;

    @Column(name = "LAST_NAME")
    private String lastName = null;

    @Column(name = "INITIALS")
    private String initials = null;

    @Column(name = "PREFIX")
    private String prefix = null;

    @Column(name = "EMAIL")
    private String email = null;

    @Column(name = "LOGIN")
    private String loginName = null;

    @Column(name = "PASSWORD")
    private String password = null;

    @Column(name = "PINCODE")
    private String pincode = null;

    @Column(name = "MAIL_BOX_ENABLED")
    private boolean mailBoxEnabled = false;

    @Column(name = "MAIL_BOX_PORT")
    private int mailPort = 25;

    @Column(name = "MAIL_BOX_SERVER")
    private String mailServer = null;

    @Column(name = "MAIL_BOX_LOGIN")
    private String mailLogin = null;

    @Column(name = "MAIL_BOX_PASSWD")
    private String mailPasswd = null;

    @Column(name = "MAIL_BOX_PROTOCOL")
    private int protocol = 1;

    @Column(name = "MAIL_BOX_SECURITY")
    private int security = 1;

    @Column(name = "MAIL_BOX_INCOMING_PORT")
    private int incomingMailPort = 25;

    @Column(name = "MAIL_BOX_INCOMING_SERVER")
    private String incomingMailServer = null;

    @Column(name = "PHONE")
    private String phone = null;

    @Column(name = "MOBILE")
    private String mobile = null;

    @Column(name = "PHONE_WORK")
    private String phoneWork = null;

    @Column(name = "LOCALE")
    private String locale = null;

    @Column(name = "GENDER")
    private int gender = Gender.UNKNOWN_INT;

    @Column(name = "BIRTHDAY")
    @Type(type = FtrLocalDateUserType.NAME)
    private FtrLocalDate birthDate = null;

    @Column(name = "DAY_OF_YEAR")
    private int dayOfYear = -1;

    @Column(name = "PWD_CHANGE", nullable = false)
    private boolean pwdChange = true;

    @Column(name = "ACTIVE", nullable = false)
    private boolean active = false;

    @Column(name = "BSN")
    private String bsn = null;


    @Column(name = "LOGIN_BLOCKED", nullable = false)
    private boolean loginBlocked = false;

    @Column(name = "LOGIN_ENABLED", nullable = true)
    private boolean loginEnabled = true;

    @Column(name = "LOGIN_SEND", nullable = false)
    private boolean loginSend = true;

    @Column(name = "ORGANISATION_ID", nullable = false)
    private long organisationId = -1;

    public UserEntity() {
        super();
    }

    @Override
    public String createLetterName() {
        final StringBuffer result = new StringBuffer();
        if (initials != null) {
            result.append(initials);
        }
        if (prefix != null) {
            if (initials != null) {
                result.append(" ");
            }
            result.append(prefix);
        }
        if (lastName != null) {
            if (result.length() > 0) {
                result.append(" ");
            }
            result.append(lastName);
        }

        return result.toString();
    }

    @Override
    public String createLetterNameNoInitals() {
        final StringBuilder result = new StringBuilder();
        if (prefix != null) {
            result.append(prefix);
        }
        if (lastName != null) {
            if (result.length() > 0) {
                result.append(" ");
            }
            result.append(lastName);
        }

        return result.toString();
    }

    @Override
    public String createPrintableName() {
        final StringBuffer result = new StringBuffer();
        if (firstName != null) {
            result.append(firstName);
        }
        if (prefix != null) {
            if (firstName != null) {
                result.append(" ");
            }
            result.append(prefix);
        }
        if (lastName != null) {
            if (result.length() > 0) {
                result.append(" ");
            }
            result.append(lastName);
        }

        return result.toString();
    }

    @Override
    public String determineFromEMailAdress() {
        return getEmail();
    }

    @Override
    public long determineOwnerId() {
        return getId();
    }

    @Override
    public String determinePeronalName() {
        return createPrintableName();
    }

    @Override
    public int determinePictureType() {
        return PictureCodes.USER;
    }

    @Override
    public int determineType() {
        return PictureCodes.USER;
    }

    @Override
    public FtrLocalDate getBirthDay() {
        return birthDate;
    }

    @Override
    public String getBsn() {
        return bsn;
    }

    public void setBsn(final String bsn) {
        this.bsn = bsn;
    }

    public int getDayOfYear() {
        return dayOfYear;
    }

    public void setDayOfYear(final int dayOfYear) {
        this.dayOfYear = dayOfYear;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    @Override
    public Gender getGender() {
        return Gender.convertGender(gender);
    }

    public void setGender(final Gender gender) {
        setGender(Gender.convertGender(gender));
    }

    public void setGender(final int gender) {
        this.gender = gender;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    @Override
    public int getIncomingMailPort() {
        return incomingMailPort;
    }

    @Override
    public void setIncomingMailPort(final int incomingMailPort) {
        this.incomingMailPort = incomingMailPort;
    }

    @Override
    public String getIncomingMailServer() {
        return incomingMailServer;
    }

    @Override
    public void setIncomingMailServer(final String incomingMailServer) {
        this.incomingMailServer = incomingMailServer;
    }

    @Override
    public String getInitials() {
        return initials;
    }

    public void setInitials(final String initials) {
        this.initials = initials;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getLocale() {
        return locale;
    }

    public void setLocale(final String locale) {
        this.locale = locale;
    }

    @Override
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(final String login) {
        loginName = login;
    }

    @Override
    public String getMailLogin() {
        return mailLogin;
    }

    @Override
    public void setMailLogin(final String mailLogin) {
        this.mailLogin = mailLogin;
    }

    @Override
    public String getMailPasswd() {
        return mailPasswd;
    }

    @Override
    public void setMailPasswd(final String mailPasswd) {
        this.mailPasswd = mailPasswd;
    }

    @Override
    public int getMailPort() {
        return mailPort;
    }

    @Override
    public void setMailPort(final int mailPort) {
        this.mailPort = mailPort;
    }

    @Override
    public String getMailServer() {
        return mailServer;
    }

    @Override
    public void setMailServer(final String mailServer) {
        this.mailServer = mailServer;
    }

    @Override
    public String getMobile() {
        return mobile;
    }

    public void setMobile(final String mobile) {
        this.mobile = mobile;
    }

    @Override
    public long getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(final long organisationId) {
        this.organisationId = organisationId;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    @Override
    public String getPhoneWork() {
        return phoneWork;
    }

    public void setPhoneWork(final String phoneWork) {
        this.phoneWork = phoneWork;
    }

    @Override
    public String getPincode() {
        return pincode;
    }

    public void setPincode(final String pincode) {
        this.pincode = pincode;
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(final String prefix) {
        this.prefix = prefix;
    }

    @Override
    public MailProtocol getProtocol() {
        return MailProtocol.convertFromCode(protocol);
    }

    @Override
    public void setProtocol(final MailProtocol protocol) {
        this.protocol = MailProtocol.convertToCode(protocol);
    }

    public String getSecToken() {
        return secToken;
    }

    public void setSecToken(final String secToken) {
        this.secToken = secToken;
    }

    @Override
    public MailSecurity getSecurity() {
        return MailSecurity.convertFromCode(security);
    }

    @Override
    public void setSecurity(final MailSecurity security) {
        this.security = MailSecurity.convertToCode(security);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public boolean isLoginBlocked() {
        return loginBlocked;
    }

    public void setLoginBlocked(final boolean loginBlocked) {
        this.loginBlocked = loginBlocked;
    }

    public boolean isLoginEnabled() {
        return loginEnabled;
    }

    public void setLoginEnabled(final boolean loginEnabled) {
        this.loginEnabled = loginEnabled;
    }

    public boolean isLoginSend() {
        return loginSend;
    }

    public void setLoginSend(final boolean loginSend) {
        this.loginSend = loginSend;
    }

    @Override
    public boolean isMailBoxEnabled() {
        return mailBoxEnabled;
    }

    @Override
    public void setMailBoxEnabled(final boolean mailBoxEnabled) {
        this.mailBoxEnabled = mailBoxEnabled;
    }

    @Override
    public boolean isPwdChange() {
        return pwdChange;
    }

    public void setPwdChange(final boolean pwdChanged) {
        pwdChange = pwdChanged;
    }

    public UserInfo createInfo() {
        UserInfo result = new UserInfo();
        result.setId(this.getId());
        result.setLabel(this.createPrintableName());
        result.setOrganisationId(this.getOrganisationId());
        return result;
    }

    public void setBirthDate(final FtrLocalDate birthDate) {
        this.birthDate = birthDate;
        if (birthDate != null) {
            final CalendarNoTimeZone helper = DateHelpers.createCalendarNoTimeZone();
            helper.setTime(birthDate);
            dayOfYear = helper.get(Calendar.DAY_OF_YEAR);
        }
    }

}
