package eu.duo.dfk.test;

import org.jboss.security.SecurityContext;
import org.jboss.security.SecurityContextAssociation;
import org.jboss.security.SecurityContextFactory;

import javax.security.auth.Subject;

public class GeneralUtils {

    public static SecurityContext fixSecurityContext(final Subject subject, final Object cred) {
        final SecurityContext old = SecurityContextAssociation.getSecurityContext();
        try {
            String domain = "";
            try {
                domain = old.getSecurityDomain();
            } catch (final NullPointerException e) {

            }
            // System.err.println("Fixing SecurityContext: " + domain);
            final SecurityContext sc = SecurityContextFactory.createSecurityContext(subject.getPrincipals().iterator().next(), cred, subject, domain);
            SecurityContextAssociation.setSecurityContext(sc);
        } catch (final Exception e) {
            e.printStackTrace(System.err);
        }

        return old;
    }

}
