package eu.duo.dfk.test;

import eu.future.earth.client.demo.TestInfo;
import eu.future.earth.client.user.SecurityPrincipalNoRoles;
import org.wildfly.security.auth.server.RealmUnavailableException;
import org.wildfly.security.auth.server.SecurityDomain;
import org.wildfly.security.auth.server.SecurityIdentity;
import org.wildfly.security.evidence.PasswordGuessEvidence;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.security.auth.login.LoginException;
import java.security.Principal;

@Stateless
public class SampleEjb {
    private final static org.apache.log4j.Logger LOGGER = eu.future.earth.logging.ExtendedLog.getLogger(SampleEjb.class);

    @Resource
    private SessionContext sessionContext;

    @PersistenceContext
    private EntityManager emBasic;

    public void register(TestInfo member) {
        System.out.println("Registering " + member.getName());
        member.setOwnerId(getUserId());
        sessionContext.getTimerService().createTimer(40, member);
    }


    @Timeout
//    @RunAs("")
    public void timeoutHandler(final Timer timer) {
        TestInfo toRun = (TestInfo) timer.getInfo();
        timer.cancel();

        long userId = getUserId();
        System.out.println("Is now " +userId);
        try {
            setLoginToUser(toRun.getOwnerId(), () -> callMethod2(toRun));
        } catch (javax.security.auth.login.LoginException e) {
            e.printStackTrace();
            return;
        }
        long userId2 = getUserId();
        System.out.println("Is now 2 " + userId2 +  " for " + toRun.getName());
    }

    private void callMethod2(TestInfo toRun) {
        long userId = getUserId();
        System.out.println("Is now " +userId +  " for " + toRun.getName());
    }


    public SecurityPrincipalNoRoles setLoginToUser(final long runAs, Runnable call) throws LoginException {
        final UserEntity userToRun = getUserEntity(runAs);
        try {
            SecurityDomain securityDomain = SecurityDomain.getCurrent();
            SecurityIdentity to = securityDomain.authenticate(userToRun.getLoginName(), new PasswordGuessEvidence(userToRun.getPassword().toCharArray()));
            to.runAs(call);
        } catch (RealmUnavailableException e) {
            LOGGER.error("Could not login for user "  + runAs,e);
            throw new LoginException("Could not long for user " + runAs);
        }
        return userToRun;
    }

    public UserEntity getUserEntity(final long userId) {
        if (userId != -1) {
            try {
                final TypedQuery<UserEntity> query = emBasic.createNamedQuery("UserEntity.FindById", UserEntity.class);
                query.setParameter("id", userId);
                return query.getSingleResult();
            } catch (final Exception e) {
                // e.printStackTrace();
                LOGGER.error("No user found for is " + userId);
            }
        }
        return null;
    }

    public long getUserId() {
        if (isLoggedIn()) {
            final Principal userPrincipal = sessionContext.getCallerPrincipal();

            final UserEntity user = getByLoginName(sessionContext.getCallerPrincipal().getName());
            if (user != null) {
                return user.getId();
            } else {
                return -1;
            }

        } else {
            return -1;
        }
    }

    public UserEntity getByLoginName(final String email) {
        final TypedQuery<UserEntity> query = emBasic.createNamedQuery("UserEntity.FindByLogin", UserEntity.class);
        query.setParameter("loginName", email);
        return query.getSingleResult();
    }

    public boolean isLoggedIn() {
        if (sessionContext != null) {
            if (sessionContext.getCallerPrincipal() != null) {
                String loginName = sessionContext.getCallerPrincipal().getName();
                if (loginName != null) {
                    if (loginName.equalsIgnoreCase("guest") || loginName.equalsIgnoreCase("anonymous")) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
