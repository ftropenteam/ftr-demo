package eu.duo.dfk.test;

import eu.future.earth.client.date.FtrLocalDate;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FtrLocalDateUserType implements UserType {
    public static final String NAME = "eu.duo.dfk.test.FtrLocalDateUserType";

    public FtrLocalDateUserType() {
    }

    public Object assemble(Serializable cached, Object owner) {
        return cached;
    }

    public Object deepCopy(Object value) {
        if (value == null) {
            return null;
        } else if (!(value instanceof FtrLocalDate)) {
            throw new UnsupportedOperationException("can't convert " + value.getClass());
        } else {
            return new FtrLocalDate(((FtrLocalDate) value).toJavaDate());
        }
    }

    public Serializable disassemble(Object value) throws HibernateException {
        return (FtrLocalDate) value;
    }

    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == null && y == null) {
            return true;
        } else {
            return x == null ? false : x.equals(y);
        }
    }

    public int hashCode(Object value) throws HibernateException {
        return value.hashCode();
    }

    @Override


    public boolean isMutable() {
        return true;
    }

    public Object replace(Object original, Object target, Object owner) {
        return original;
    }

    public Class returnedClass() {
        return FtrLocalDate.class;
    }

    public int[] sqlTypes() {
        return new int[]{91};
    }

    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        Date value = rs.getDate(names[0]);
        return value == null ? null : new FtrLocalDate(value.getTime());
    }

    public void nullSafeSet(PreparedStatement stmt, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if (value == null) {
            stmt.setDate(index, (Date) null);
        } else if (!(value instanceof FtrLocalDate)) {
            throw new UnsupportedOperationException("can't convert " + value.getClass());
        } else {
            stmt.setDate(index, new Date(((FtrLocalDate) value).toJavaDate().getTime()));
        }
    }
}
